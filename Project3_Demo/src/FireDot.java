import java.util.ArrayList;
import java.util.List;

import helper.DeltaTime;

public class FireDot
{
	// Max 5 stacks of the damage can be applied
	private static final int MAX_STACKS = 5;
	// Total damage will occur over the course of 10secs
	private static final int duration = 10000;
	private float damage;
	private long expirationTime;
	private final List<Float> stacks = new ArrayList<>();
	private boolean expired;
	private boolean shouldUpdateDamage = false;
	
	public FireDot()
	{
		
	}
	
	public void update()
	{
		if (!expired)
		{
			// If current time is greater than 
			// expiration time or stacks has no 
			// values, then set to inactive
			// and reset all values
			if (System.currentTimeMillis() > expirationTime || stacks.size() == 0)
			{
				damage = 0.0f;
				expired = true;
				stacks.clear();
			}
		}
		
		if (shouldUpdateDamage)
		{
			float tempDmg = 0f;
			
			for (Float value : stacks)
			{
				tempDmg += value;
			}
			
			damage = tempDmg;
			
			shouldUpdateDamage = false;
		}
	}
	
	public boolean isActive()
	{
		return !expired;
	}
	
	public void addStack(float amount)
	{
		expired = false;
		
		// If not at max stacks, add
		// one to the list
		if (stacks.size() < MAX_STACKS)
		{
			stacks.add(amount);
			shouldUpdateDamage = true;
		}
		else
		{
			// Check if the new amount is greater
			// than any of the old and swap if so
			for (int i = 0; i < stacks.size(); i++)
			{
				if (stacks.get(i) < amount)
				{
					stacks.set(i, amount);
					shouldUpdateDamage = true;
					break;
				}
			}
		}
		
		// Refresh the expiration time
		// whenever a stack is added
		refreshTime();
	}
	
	public boolean hasMaxStacks()
	{
		return stacks.size() == MAX_STACKS;
	}
	
	public boolean isAboutToExpire()
	{
		return expirationTime - System.currentTimeMillis() < 1000;
	}
	
	public float getElapsedDamage()
	{
		// Have to calculate the duration
		// with regard to the time multiplier
		float tempDur = (float)duration * DeltaTime.getMultiplier();
		
		return (float) (DeltaTime.getDeltaTime() / 1000.0f * (damage / (tempDur / 1000f)));
	}
	
	private void refreshTime()
	{
		// Have to calculate the duration
		// with regard to the time multiplier
		float tempDur = (float)duration * DeltaTime.getMultiplier();
		
		expirationTime = System.currentTimeMillis() + Math.round(tempDur);
	}
}
