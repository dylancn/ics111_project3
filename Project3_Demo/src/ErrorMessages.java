import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import helper.Pair;

public class ErrorMessages
{
	private static final int MESSAGE_DURATION = 3000;
	private static final int FONT_SIZE = 30;
	private static final int MAX_MESSAGES = 5;
	
	private final List<Pair<EZText, Long>> myMessages = new ArrayList<>();
	private final List<Pair<EZText, Long>> myTrashMessages = new ArrayList<>();
	private final int x;
	private final int WIDTH;
	
	/**
	 * 
	 * @param _width The width of the screen
	 */
	public ErrorMessages(int _width)
	{
		x = _width / 2;
		WIDTH = _width;
	}
	
	public void update()
	{
		// Once there's more than MAX_ERRORMESSAGES on the
		// screen, start removing from the stop.
		if (myMessages.size() > MAX_MESSAGES)
		{
			int removeCount = myMessages.size() - MAX_MESSAGES;
			//amountOfFinishedMessage = removeCount;
			
			for (int i = 0; i < removeCount; i++)
			{
				// This will force the time check to equal true
				// causing this message to be added to the trash
				myMessages.get(i).setSecond(System.currentTimeMillis());
			}
		}
		
		int amountOfFinishedMessage = 0;
		// Update each message
		for (int i = 0; i < myMessages.size(); i++)
		{
			Pair<EZText, Long> current = myMessages.get(i);
			
			if (System.currentTimeMillis() < current.getSecond())
			{
				// Don't translate the top most element
				if (amountOfFinishedMessage > 0 && i != 0)
				{
					translateMessage(current, amountOfFinishedMessage);
				}
			}
			else
			{
				amountOfFinishedMessage++;
				myTrashMessages.add(current);
			}
		}
		
		// Remove trash messages from messages
		myMessages.removeAll(myTrashMessages);
		
		// Destroy each message in trash
		// Loop backwards to avoid error
		for (int i = myTrashMessages.size() - 1; i >= 0; i--)
		{
			destroy(myTrashMessages.get(i));
			myTrashMessages.remove(i);
		}
	}
	
	/**
	 * Translates the message upward
	 * @param object The Pair<EZText, Long>
	 * @param _amountOfFinishedMessages The amount of messages that have finished since updating
	 */
	private void translateMessage(Pair<EZText, Long> object, int _amountOfFinishedMessages)
	{
		object.getFirst().translateBy(0, -FONT_SIZE * _amountOfFinishedMessages);
	}
	
	/**
	 * Removes the EZText from the screen
	 * @param object
	 */
	private void destroy(Pair<EZText, Long> object)
	{
		EZ.removeEZElement(object.getFirst());
	}
	
	/**
	 * Add a message to the screen
	 * @param message The message to be displayed
	 */
	public void add(String message)
	{
		EZText temp = EZ.addText(x, (FONT_SIZE * myMessages.size()) + FONT_SIZE, message, Color.RED, FONT_SIZE);
		
		// Just in case, check if the message is bigger
		// than the screen width. If so, start reducing
		// its size until it fits
		if (temp.getWidth() > WIDTH)
		{
			int fontSize = FONT_SIZE;
			while (temp.getWidth() > WIDTH)
			{
				temp.setFontSize(fontSize -= 5);
			}
		}
		
		// First value of the pair holds the EZText object
		// Second value of the pair holds the expiration time
		myMessages.add(new Pair<EZText, Long>(temp, System.currentTimeMillis() + MESSAGE_DURATION));
	}
}
