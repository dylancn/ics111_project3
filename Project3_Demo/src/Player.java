
public class Player
{
	private final int MAX_LIFE = 10;
	
	private int lives;
	
	private Inventory inventory;
	
	public Player()
	{
		lives = MAX_LIFE;
		inventory = new Inventory();
	}
	
	// We can add or subtract lives from
	// the player. This will be the lose
	// condition for the game
	public void changeLives(long amount)
	{
		if (lives + amount > Integer.MAX_VALUE)
		{
			lives = Integer.MAX_VALUE;
		}
		else if (lives + amount < 0)
		{
			lives = -1;
		}
		else
			lives += amount;
	}
	
	public int getLives()
	{
		return lives;
	}
	
	public Inventory getInventory()
	{
		return inventory;
	}
	
	public int getMaxLives()
	{
		return MAX_LIFE;
	}
}
