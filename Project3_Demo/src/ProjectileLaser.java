import java.awt.Color;

import enums.ProjectileType;
import helper.StopWatch;

public class ProjectileLaser extends Projectile
{
	private final int LINE_THICKNESS = 5;
	
	private EZLine line;
	private int thickness;
	private StopWatch fadeTimer = new StopWatch();
	
	private static ProjectileType type = ProjectileType.Laser;
	
	public ProjectileLaser()
	{
		super(type);
		
		thickness = LINE_THICKNESS;
	}
	
	@Override
	public void update()
	{
		if (!fadeTimer.isRunning())
		{
			fadeTimer.start();
		}
		
		if (line == null)
		{
			line = EZ.addLine(getOffsetX(), getOffsetY(), getTarget().getImage().getXCenter(), getTarget().getImage().getYCenter(), Color.red, thickness);
			getTarget().causeDamage(getParent().getDamage(), type.getElementalType());
		}
		
		if (line.getThickness() > 1)
		{
			if (fadeTimer.getElapsedTime().toMillis() > 50)
			{
				fadeTimer.restart();
				line.setThickness(--thickness);
			}
		}
		else
		{
			fadeTimer.stop();
			isFinished = true;
			destroy();
		}
	}
	
	@Override
	public void destroy()
	{
		EZ.removeEZElement(line);
		line = null;
		thickness = LINE_THICKNESS;
	}
}
