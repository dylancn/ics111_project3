import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import enums.EffectsType;

public class ProjectileEffectManager
{
	private final List<ProjectileEffect> myEffects;
	
	public ProjectileEffectManager()
	{
		myEffects = new ArrayList<>();
	}
	
	/**
	 * Updates all active Projectile Effects
	 */
	public void update()
	{
		for (ProjectileEffect projectileEffect : myEffects)
		{
			if (projectileEffect.isFinished())
			{
				projectileEffect.makeInactive();
				continue;
			}
			
			projectileEffect.update();
		}
	}
	
	/**
	 * Activates a dormant effect or makes a new one if none available.
	 * @param _effectType The EffectType of the effect that should be activated
	 * @param location The location the effect should appear at
	 * @return True if an effect was reused or made successfully, false otherwise
	 */
	public boolean activateOrMakeNextAvailable(EffectsType _effectType, Point location)
	{
		// Active a projectile effect already made
		// and return
		for (ProjectileEffect projectileEffect : myEffects)
		{
			if (projectileEffect.getEffectsType().equals(_effectType))
			{
				if (!projectileEffect.isActive())
				{
					projectileEffect.makeActive(location);
					return true;
				}
			}
		}
		
		// Make a new projectile if needed
		ProjectileEffect myEffect = new ProjectileEffect(_effectType);
		myEffect.makeActive(location);
		return myEffects.add(myEffect);
	}
}
