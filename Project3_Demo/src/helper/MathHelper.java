package helper;

import java.awt.Point;
import java.awt.geom.Point2D;

public class MathHelper
{
	/**
	 * Calculates the angle from centerPt to targetPt in degrees.
	 * The return should range from [0,360), rotating CLOCKWISE, 
	 * 0 and 360 degrees represents NORTH,
	 * 90 degrees represents EAST, etc...
	 *
	 * Assumes all points are in the same coordinate space.  If they are not, 
	 * you will need to call SwingUtilities.convertPointToScreen or equivalent 
	 * on all arguments before passing them  to this function.
	 *
	 * @param centerPt   Point we are rotating around.
	 * @param targetPt   Point we want to calcuate the angle to.  
	 * @return angle in degrees.  This is the angle from centerPt to targetPt.
	 */
	public static double calcRotationAngleInDegrees(Point centerPt, Point targetPt)
	{
	    // calculate the angle theta from the deltaY and deltaX values
	    // (atan2 returns radians values from [-PI,PI])
	    // 0 currently points EAST.  
	    // NOTE: By preserving Y and X param order to atan2,  we are expecting 
	    // a CLOCKWISE angle direction.  
	    double theta = Math.atan2(targetPt.y - centerPt.y, targetPt.x - centerPt.x);

	    // rotate the theta angle clockwise by 90 degrees 
	    // (this makes 0 point NORTH)
	    // NOTE: adding to an angle rotates it clockwise.  
	    // subtracting would rotate it counter-clockwise
	    //theta += Math.PI/2.0;

	    // convert from radians to degrees
	    // this will give you an angle from [0->270],[-180,0]
	    double angle = Math.toDegrees(theta);

	    // convert to positive range [0-360)
	    // since we want to prevent negative angles, adjust them now.
	    // we can assume that atan2 will not return a negative value
	    // greater than one partial rotation
	    if (angle < 0) {
	        angle += 360;
	    }

	    return angle;
	}
	
	/**
	 * Calculates the angle from centerPt to targetPt in degrees.
	 * The return should range from [0,360), rotating CLOCKWISE, 
	 * 0 and 360 degrees represents NORTH,
	 * 90 degrees represents EAST, etc...
	 *
	 * Assumes all points are in the same coordinate space.  If they are not, 
	 * you will need to call SwingUtilities.convertPointToScreen or equivalent 
	 * on all arguments before passing them  to this function.
	 *
	 * @param centerPt   Point we are rotating around.
	 * @param targetPt   Point we want to calcuate the angle to.  
	 * @return angle in degrees.  This is the angle from centerPt to targetPt.
	 */
	public static double calcRotationAngleInDegrees(Point2D.Double centerPt, Point2D.Double targetPt)
	{
	    // calculate the angle theta from the deltaY and deltaX values
	    // (atan2 returns radians values from [-PI,PI])
	    // 0 currently points EAST.  
	    // NOTE: By preserving Y and X param order to atan2,  we are expecting 
	    // a CLOCKWISE angle direction.  
	    double theta = Math.atan2(targetPt.y - centerPt.y, targetPt.x - centerPt.x);

	    // rotate the theta angle clockwise by 90 degrees 
	    // (this makes 0 point NORTH)
	    // NOTE: adding to an angle rotates it clockwise.  
	    // subtracting would rotate it counter-clockwise
	    //theta += Math.PI/2.0;

	    // convert from radians to degrees
	    // this will give you an angle from [0->270],[-180,0]
	    double angle = Math.toDegrees(theta);

	    // convert to positive range [0-360)
	    // since we want to prevent negative angles, adjust them now.
	    // we can assume that atan2 will not return a negative value
	    // greater than one partial rotation
	    if (angle < 0) {
	        angle += 360;
	    }

	    return angle;
	}
	
	/*
	 * Get a point between the origin and target
	 * thats distance units away
	 */
	public static Point2D.Double getPoint(Point origin, Point target, double distance)
	{
		double originX = origin.getX();
		double originY = origin.getY();
		double targetX = target.getX();
		double targetY = target.getY();

		double dX = originX - targetX;
		double dY = originY - targetY;
		double magnitude = Math.sqrt(Math.pow(dX, 2) + Math.pow(dY, 2));

		double uX = dX / magnitude;
		double uY = dY / magnitude;

		double newX = uX * distance;
		double newY = uY * distance;
		
		return new Point2D.Double(originX - newX, originY - newY); 
	}
	
	/*
	 * Same as the above function... just more suited
	 * toward the use of finding an offset position for
	 * firing projectiles a set distance away from
	 * the center point
	 */
	public static Point calculateOffsetPoint(int _startX, int _startY, int _endX, int _endY, int offset)
	{
		int objX = _startX;
		int objY = _startY;
		int targetX = _endX;
		int targetY = _endY;

		int dX = targetX - objX;
		int dY = targetY - objY;
		double magnitude = Math.sqrt((dX * dX) + (dY * dY));

		double uX = dX / magnitude;
		double uY = dY / magnitude;

		double finalX = uX * offset;
		double finalY = uY * offset;
		
		return new Point((int)finalX, (int)finalY);
	}
	
	/**
	 * 
	 * @param origin Start point
	 * @param target End point
	 * @return the distance from start to end point
	 */
	public static double getDistance(Point origin, Point target)
	{
		return Point.distance(origin.getX(), origin.getY(), target.getX(), target.getY());
	}
	
	/**
	 * 
	 * @param origin Start point
	 * @param target End point
	 * @return the distance from start to end point
	 */
	public static double getDistance(int x1, int y1, int x2, int y2)
	{
		return Point.distance(x1, y1, x2, y2);
	}
	
	/**
	 * 
	 * @param distance The distance of the next waypoint
	 * @param angle The angle to create the waypoint at
	 * @return The point of the next waypoint
	 */
	public static Point2D.Double createNextWayPoint(double distance, double angle)
	{
		double x = distance * (Math.cos(Math.toRadians(angle)));
		double y = distance * (Math.sin(Math.toRadians(angle)));
		
		return new Point2D.Double(x, y);
	}
}
