package helper;

import java.time.Duration;
import java.time.Instant;

public class Pacing
{
	//private Instant currentTime = Instant.now();
	private Instant lastUpdate = Instant.now();
	private Duration lastDelta;
	private Instant currentTime = Instant.now();
	private Duration currentDelta;
	
	private float multiplier = 1f;
	
	public float getDeltaTime()
	{
		Duration delta;
		currentTime = Instant.now();
		currentDelta = Duration.between(lastUpdate, currentTime);
		
		if (lastDelta != null)
		{
			delta = currentDelta.minus(lastDelta);
		}
		else 
		{
			delta = Duration.ZERO;
			lastDelta = delta;
		}
		//System.out.println("ms: " + delta.toMillis() + " multiplier: " + multiplier + " ms * multiplier: " + delta.toMillis() * multiplier);
		//System.out.println(delta.toMillis() * (long)multiplier);
		return delta.toMillis() * multiplier;
	}
	
	public void resetTime()
	{
		lastDelta = currentDelta;
	}
	
	public void setMultiplier(float multi)
	{
		multiplier = multi;
	}
	
	public float getMultiplier()
	{
		return multiplier;
	}
}
