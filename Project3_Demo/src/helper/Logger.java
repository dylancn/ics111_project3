package helper;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class Logger
{
	private boolean printMessage = true;
	private boolean allowDuplicate = false;
	private String lastMessage = "";
	private String title;
	//private static LogLevel logLevel = LogLevel.Info;
	
	enum LogLevel
	{
		Info,
		Verbose,
		Debug,
	}
	
	public Logger(String _title)
	{
		title = _title;
		
		if (title == null)
		{
			title = "Info";
		}
		
	}
	
	public void log(String msg)
	{
		if (printMessage)
		{
			if (!lastMessage.equals(msg) || allowDuplicate)
			{
				System.out.println("[" + title + "] [" + currentTime() + "] " + msg);
				lastMessage = msg;
			}
		}
	}
	
	public void setAllowDuplicate(boolean allow)
	{
		allowDuplicate = allow;
	}
	
	public boolean getAllowDuplicate()
	{
		return allowDuplicate;
	}
	
	public void setPrintMessage(boolean print)
	{
		printMessage = print;
	}
	
	public boolean getPrintMessage()
	{
		return printMessage;
	}
	
	private static String currentTime()
    {
        LocalDateTime datetime = LocalDateTime.ofInstant(Instant.now(), ZoneOffset.systemDefault());
        DateTimeFormatter formatted = DateTimeFormatter.ofPattern("hh:mm:ss.SSS");
        return formatted.format(datetime);
    }
}
