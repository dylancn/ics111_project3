package helper;

import java.time.Duration;
import java.time.Instant;

public class DeltaTime
{
	private static Instant lastUpdate = Instant.now();
	private static Instant currentTime;
	private static Duration delta;
	
	private static float MAX_MULTIPLIER = 3.0f;
	private static float MIN_MULTIPLIER = 1.0f;
	private static float multiplier = 2f;
	
	/**
	 * Calculates the delta time
	 * @return Delta time
	 */
	private static Duration getDelta()
	{
		currentTime = Instant.now();
		
		Duration d = Duration.between(lastUpdate, currentTime);
		
		lastUpdate = Instant.now();
		
		return d;
	}
	 
	/**
	 * Updates DeltaTime
	 */
	public static void update()
	{
		delta = getDelta();
	}
	
	/**
	 * 
	 * @return The time since DeltaTime was last updated
	 */
	public static long getDeltaTime()
	{
		return (long) (delta.toMillis() * multiplier);
	}
	
	/**
	 * 
	 * @return The DeltaTime multiplier
	 */
	public static float getMultiplier()
	{
		return multiplier;
	}
	
	/**
	 * Changes the multiplier to the given value without any constraints.
	 * @param multi The value to change the multiplier to.
	 */
	public static void setMultiplier(float multi)
	{
		multiplier = multi;
	}
	
	/**
	 * Decrease multiplier with restraints
	 * @param amount The amount to decrease the multiplier by
	 * @return True if the value was changed, false otherwise.
	 */
	public static boolean decreaseMultiplier(float amount)
	{
		float temp = multiplier - amount;
		
		if (multiplier == MIN_MULTIPLIER)
			return false;
		
		if (temp < MIN_MULTIPLIER)
		{
			multiplier = MIN_MULTIPLIER;
			return false;
		}
		
		if (temp >= MIN_MULTIPLIER)
		{
			multiplier = temp;
			return true;
		}
		
		return false;
	}
	
	/**
	 * Increase multiplier with restraints
	 * @param amount The amount to increase the multiplier by
	 * @return True if the value was changed, false otherwise.
	 */
	public static boolean increaseMultiplier(float amount)
	{
		float temp = multiplier + amount;
		
		if (multiplier == MAX_MULTIPLIER)
			return false;
		
		if (temp > MAX_MULTIPLIER)
		{
			multiplier = MAX_MULTIPLIER;
			return false;
		}
		
		if (temp <= MAX_MULTIPLIER)
		{
			multiplier = temp;
			return true;
		}
		
		return false;
	}
}
