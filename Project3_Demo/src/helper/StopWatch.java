package helper;
import java.time.Duration;
import java.time.Instant;

public class StopWatch
{
	private Instant startTime;
	private Instant endTime;
	private Duration timeDuration;
	private boolean isRunning = false;

	/*
	 * Start the stopwatch.
	 */
	public void start()
	{
		if (isRunning)
		{
			return;
		}
		
		startTime = Instant.now();
		isRunning = true;
	}

	/*
	 * This functions stop the stopwatch from counting. You can start 
	 * the stop watch again using the start() function. Unless you call
	 * the reset() function, time will be saved.
	 */
	public void stop()
	{
		if (isRunning)
		{
			endTime = Instant.now();
			Duration timeDiff = Duration.between(startTime, endTime);
			
			// Add the amount of time that has passed between
			// the start of the stopwatch and the end of the stopwatch
			// if we don't already have a timeDuration, just set it
			if (timeDuration == null)
			{
				timeDuration = timeDiff;
			}
			else
			{
				timeDuration = timeDuration.plus(timeDiff);
			}
			
			isRunning = false;
		}
	}
	
	/*
	 * Returns whether or not the stopwatch is running.
	 */
	public boolean isRunning()
	{
		return isRunning;
	}

	/*
	 * This function resets the stopwatch. It will stop the stopwatch
	 * if it's still running and it will reset any saved time that
	 * it may contain.
	 */
	public void reset()
	{
		if (isRunning)
		{
			stop();
		}
		
		// Ensure we fully reset all of the time values
		startTime = null;
		endTime = null;
		timeDuration = null;
	}

	/*
	 * This function will perform reset() and start(). It'll essentially
	 * stop and erase any saved time in the stopwatch and start it again.
	 */
	public void restart()
	{
		reset();
		start();
	}

	/*
	 * Returns a duration.
	 */
	public Duration getElapsedTime()
	{
		// Stopwatch hasn't been stopped, just return
		// the duration since starting the stopwatch and now
		if (isRunning)
		{
			return (Duration.between(startTime, Instant.now()));
		}

		// Stopwatch is stopped, return total duration
		return timeDuration;
	}
}
