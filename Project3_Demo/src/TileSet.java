import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

public class TileSet
{
	private final Cell[][] tiles;
	private final Point worldSize;
	private final int cellRadius;
	private final int cellDiameter;
	private final int cellSizeX;
	private final int cellSizeY;
	//private static Random random = new Random();
	
	/**
	 * Creates a new TileSet
	 * @param worldSizeX The width of the map
	 * @param worldSizeY The height of the map
	 * @param _cellDiameter How big each cell should be
	 */
	public TileSet(int worldSizeX, int worldSizeY, int _cellDiameter)
	{
		worldSize = new Point(worldSizeX, worldSizeY);
		cellDiameter = _cellDiameter;
		cellSizeX = (int)worldSize.x / cellDiameter;
		cellSizeY = (int)worldSize.y / cellDiameter;
		cellRadius = cellDiameter / 2;
		
		tiles = new Cell[cellSizeX][cellSizeY];
		
		createGrid();
	}
	
	private void createGrid()
	{
		for (int x = 0; x < cellSizeX; x++)
		{
			for (int y = 0; y < cellSizeY; y++)
			{
				Point point = new Point(x * cellDiameter, y * cellDiameter);
				
				// use this if you want random walkable cells
				//boolean walkable = random.nextDouble() < 0.80;
				boolean walkable = true;
				tiles[x][y] = new Cell(walkable, point, x, y, cellRadius);
				
				// Color only the un-walkable cells
				if (!walkable)
				{
					// Comment this out to increase speed, if you do not need to visualize walkable tiles
					EZ.addRectangle(tiles[x][y].getCenterPoint().x, tiles[x][y].getCenterPoint().y, cellDiameter, cellDiameter, Color.BLACK, true);
				}
				
				// offset grid location so it's not in the middle
				//EZ.addText(x * cellDiameter + cellRadius, y * cellDiameter + 10, x + ", " + y, Color.magenta, 7);
			}
		}
	}
	
	/**
	 * Get the cell position from a given x, y value
	 * @param x The x position
	 * @param y The y position
	 * @return Returns the cell that includes the point (x, y)
	 */
	public Cell getCellFromPosition(int x, int y)
	{
		// get percentage
		// cast to float to avoid integer division
		float posX = (float)x / worldSize.x;
		float posY = (float)y / worldSize.y;
		
		int cellX = (int) Math.floor((cellSizeX) * posX);
		int cellY = (int) Math.floor((cellSizeY) * posY);
		
		return tiles[cellX][cellY];
	}
	
	/**
	 * Get the cell position from a given Point
	 * @param pt The point
	 * @return Returns the cell that includes the point (x, y)
	 */
	public Cell getCellFromPosition(Point pt)
	{
		// get percentage
		// cast to float to avoid integer division
		float posX = (float)(pt.x) / worldSize.x;
		float posY = (float)(pt.y) / worldSize.y;
		
		int cellX = (int) Math.floor((cellSizeX) * posX);
		int cellY = (int) Math.floor((cellSizeY) * posY);
		
		return tiles[cellX][cellY];
	}
	
	public List<Cell> getCells()
	{
		List<Cell> list = new ArrayList<>();
		for (int x = 0; x < cellSizeX; x++)
		{
			for (int y = 0; y < cellSizeY; y++)
			{
				list.add(tiles[x][y]);
			}
		}
		
		return list;
	}
	
	/**
	 * Get the neighbors of a given cell
	 * @param cell The cells to find neighbors around
	 * @param allowDiag Allow diagonal pathing
	 * @return
	 */
	public List<Cell> getNeighbors(Cell cell, boolean allowDiag)
	{
		List<Cell> neighbors = new ArrayList<>();

		if (allowDiag)
		{
			// Gets all the cells surrounding current cell
			for (int i = -1; i <= 1; i++)
			{
				for (int j = -1; j <= 1; j++)
				{
					// this is the cell we're checking for neighbors
					// skip this cell
					if (i == 0 && j == 0)
						continue;
					
					// Avoid diagonal movement between 2 obstacles
					// a b c
					// d O e
					// f g h
					// Seeking neighbors around O... Assume b & e are obstacles. 
					// If we're checking to see if c is a valid cell, this
					// function will return false. However, assume only b is an obstacle
					// this function will return true.
					if (!isDiagonalNeighborValid(i, j, cell))
					{
						continue;
					}
					
					int neighborX = cell.getX() + i;
					int neighborY = cell.getY() + j;
					
					// Ensure the cell exists
					if (neighborX >= 0 && neighborX < cellSizeX && neighborY >= 0 && neighborY < cellSizeY)
					{
						neighbors.add(tiles[neighborX][neighborY]);
					}
				}
			}
		}
		else
		{
			// Only add a neighbor if they exist
			
			// get the cell above the current cell
			if (cell.getY() - 1 >= 0)
				neighbors.add(tiles[cell.getX()][cell.getY() - 1]);
			// get the cell below the current cell
			if (cell.getY() + 1 < cellSizeY)
				neighbors.add(tiles[cell.getX()][cell.getY() + 1]);
			// get the cell to the left of the current cell
			if (cell.getX() - 1 >= 0)
				neighbors.add(tiles[cell.getX() - 1][cell.getY()]);
			// get the cell to the right of the current cell
			if (cell.getX() + 1 < cellSizeX)
				neighbors.add(tiles[cell.getX() + 1][cell.getY()]);
		}
		
		return neighbors;
	}
	
	private boolean isDiagonalNeighborValid(int i, int j, Cell cell)
	{
		int nX;
		int nY;
		int mX;
		int mY;
		// Avoid diagonal movement between 2 obstacles
		// top left corner
		if (i == -1 && j == -1)
		{
			mX = cell.getX() - 1;
			mY = cell.getY();
			nX = cell.getX();
			nY = cell.getY() - 1;
			
			if (mX >= 0 && mX < cellSizeX && nX >= 0 && nX < cellSizeX && nY >= 0 && nY < cellSizeY && mY >= 0 && mY < cellSizeY)
			{
				if (!tiles[mX][mY].isWalkable() && !tiles[nX][nY].isWalkable())
				{
					//System.out.println("Skipping top left diagonal");
					return false;
				}
			}
		}
		// bottom left corner
		else if (i == -1 && j == 1)
		{
			mX = cell.getX() - 1;
			mY = cell.getY();
			nX = cell.getX();
			nY = cell.getY() + 1;
			
			if (mX >= 0 && mX < cellSizeX && nX >= 0 && nX < cellSizeX && nY >= 0 && nY < cellSizeY && mY >= 0 && mY < cellSizeY)
			{
				if (!tiles[mX][mY].isWalkable() && !tiles[nX][nY].isWalkable())
				{
					//System.out.println("Skipping bottom left diagonal");
					return false;
				}
			}
		}
		// top right corner
		else if (i == 1 && j == -1)
		{
			mX = cell.getX();
			mY = cell.getY() - 1;
			nX = cell.getX() + 1;
			nY = cell.getY();
			
			if (mX >= 0 && mX < cellSizeX && nX >= 0 && nX < cellSizeX && nY >= 0 && nY < cellSizeY && mY >= 0 && mY < cellSizeY)
			{
				if (!tiles[mX][mY].isWalkable() && !tiles[nX][nY].isWalkable())
				{
					//System.out.println("Skipping top right diagonal");
					return false;
				}
			}
		}
		// bottom right corner
		else if (i == 1 && j == 1)
		{
			mX = cell.getX() + 1;
			mY = cell.getY();
			nX = cell.getX();
			nY = cell.getY() + 1;
			
			if (mX >= 0 && mX < cellSizeX && nX >= 0 && nX < cellSizeX && nY >= 0 && nY < cellSizeY && mY >= 0 && mY < cellSizeY)
			{
				if (!tiles[mX][mY].isWalkable() && !tiles[nX][nY].isWalkable())
				{
					//System.out.println("Skipping bottom right diagonal");
					return false;
				}
			}
		}
		
		return true;
	}
	
	public List<Cell> getTowerAttackCells(Cell parentCell, int attackRange)
	{
		List<Cell> neighbors = new ArrayList<>();

		// Gets all the cells surrounding current cell
		for (int i = -attackRange; i <= attackRange; i++)
		{
			for (int j = -attackRange; j <= attackRange; j++)
			{
				// this is the cell we're checking for neighbors
				// skip this cell
				if (i == 0 && j == 0)
					continue;
				
				// Don't include the outer-most corners
				// to the attackable cells... to some what
				// create an attackable circle
				if (attackRange > 1)
				{
					if ((i == -attackRange && (j == -attackRange || j == attackRange)) || (i == attackRange && (j == -attackRange || j == attackRange)))
					{
						continue;
					}
				}
				
				int neighborX = parentCell.getX() + i;
				int neighborY = parentCell.getY() + j;
				
				// Ensure the cell exists
				if (neighborX >= 0 && neighborX < cellSizeX && neighborY >= 0 && neighborY < cellSizeY)
				{
					neighbors.add(tiles[neighborX][neighborY]);
				}
			}
		}
		
		return neighbors;
	}
	
	/**
	 * Return the array of Cells
	 * @return Returns Cell[][]
	 */
	public Cell[][] getTiles()
	{
		return tiles;
	}
	
}
