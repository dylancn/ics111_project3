import java.awt.Point;
import java.awt.geom.Point2D;

import enums.ProjectileType;
import helper.DeltaTime;
import helper.MathHelper;

public class ProjectileIce extends Projectile
{
	
	private static final int width = 48;
	private static final int height = 19;
	
	private int xt = 0;
	private int yt = 0;
	private int xb = width;
	private int yb = height;

	public ProjectileIce()
	{
		super(ProjectileType.Ice);
		getProjectileImg().setFocus(0, 0, width, height);
	}

	@Override
	public void update()
	{
		if (!isFinished && isActive())
		{
			EZImage projectile = super.getProjectileImg();
			Point projectilePos = new Point(projectile.getXCenter(), projectile.getYCenter());
			Point target = new Point(getTarget().getImage().getXCenter(), getTarget().getImage().getYCenter());
			
			double distance = MathHelper.getDistance(new Point(target.x, target.y), new Point(projectile.getXCenter(), projectile.getYCenter()));
			if (distance < projectile.getWidth() / 3)
			{
				getTarget().causeDamage(getParent().getDamage(), getType().getElementalType());
				getTarget().addIceDebuff();
				isFinished = true;
				return;
			}
			
			double angle = MathHelper.calcRotationAngleInDegrees(projectilePos, target);
			double distanceToTarget = Point2D.distance(projectilePos.getX(), projectilePos.getY(), target.getX(), target.getY());
			double distanceToNextWayPoint = Math.min(distanceToTarget, DeltaTime.getDeltaTime() / (float)1000 * getSpeed());
			
			Point2D.Double destination = MathHelper.createNextWayPoint(distanceToNextWayPoint, angle);
			
			yt += height;
			yb += height;
			if (yt >= height * 6)
			{
				xt = 0;
				yt = 0;
				xb = width;
				yb = height;
			}
			
			projectile.setFocus(xt, yt, xb, yb);
			
			// Rotate 
			double rotation = MathHelper.calcRotationAngleInDegrees(new Point(projectile.getXCenter(), projectile.getYCenter()), target);
			if (projectile.getRotation() != rotation)
			{
				projectile.rotateTo(rotation);
			}
			
			projectile.translateBy(destination.x, destination.y);
		}
	}
}
