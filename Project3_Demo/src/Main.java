
import enums.GameState;
import helper.DeltaTime;
import helper.Logger;

/*
 * http://freegameassets.blogspot.com/2015/02/free-tower-defence-sets-this-free-tower.html
 * http://gamedev.stackexchange.com/questions/854/what-are-good-games-to-earn-your-wings-with
 * https://www.google.com/search?q=tower+sprite&source=lnms&tbm=isch&sa=X&ved=0ahUKEwi3qf_bzO7SAhUIs1QKHR1DCBwQ_AUICCgB&biw=1920&bih=965#imgrc=YeqHrNJ-sm6HVM:
 * https://www.google.com/search?q=tower+defense+sprites&tbm=isch&imgil=diJb4f4yFNPGIM%253A%253Bcg31oCOce_3ezM%253Bhttps%25253A%25252F%25252Fwww.pinterest.com%25252Fmaridornellas%25252Ftower-defense-weapons-ideas%25252F&source=iu&pf=m&fir=diJb4f4yFNPGIM%253A%252Ccg31oCOce_3ezM%252C_&usg=__Z_AWlVyNBLniKrFuupDdMjobQng%3D&biw=1920&bih=1014&ved=0ahUKEwjkiMLVo-7SAhVI-GMKHWW_Cw8QyjcILg&ei=Ip3UWOTvIcjwjwPl_q54#tbm=isch&q=2d+monster+sprite&*&imgrc=CMO20E8IjDl8IM:
 * http://www.riemers.net/eng/Tutorials/XNA/Csharp/Series2D/Angle_to_Direction.php
 * http://zetcode.com/tutorials/javagamestutorial/collision/
 */

/*
 * http://buttonoptimizer.com/
 * Used this website to make the start menu buttons.
 * 
 * Font: Arial
 * Font size: 35px
 * Horizontal padding: 24px
 * Vertical padding: 12px
 * Border radius: 8px
 * Background top color: ff4a4a
 * Background bottom color: 992727
 * Border color: a12727
 * 
 * Character Sprite generator
 * http://gaurav.munjal.us/Universal-LPC-Spritesheet-Character-Generator/
 * 
 * Combine multiple images into a sprite sheet
 * http://css.spritegen.com/
 * http://zerosprites.com/
 * 
 */

public class Main
{
	private static final int EZ_HEIGHT = 800;
	private static final int EZ_WIDTH = 1000;
	private static final int CELL_SIZE = 50;
	
	public static void main(String[] args)
	{
		EZ.initialize(EZ_WIDTH, EZ_HEIGHT);
		
		GameStateManager gsm = new GameStateManager();
		Game game = null;
		MainMenu menu = new MainMenu(EZ_WIDTH, EZ_HEIGHT, gsm);
		EndScreen endScreen = new EndScreen(EZ_WIDTH, EZ_HEIGHT, gsm);
		
		Logger logger = new Logger("Main.java");
		logger.setAllowDuplicate(true);
		
		while (true)
		{
			// Call this every iteration to get the time
			// delta for updating the game
			DeltaTime.update();
			
			// Output if we get high delta times...
			// need to optimize if this occurs
			// regularly
			if (DeltaTime.getDeltaTime() / DeltaTime.getMultiplier() > 100)
				logger.log("High delta time: " + Integer.toString((int)(DeltaTime.getDeltaTime() / DeltaTime.getMultiplier())) + "ms");
			
			switch (gsm.getState())
			{
				case NewGame:
					EZ.removeAllEZElements();
					game = new Game(EZ_WIDTH, EZ_HEIGHT, CELL_SIZE, gsm);
					gsm.setState(GameState.Game);
					break;
					
				case MainMenu:
					EZ.refreshScreen();
					menu.update();
					break;
				
				case Game:
				case Pause:
					game.update();
					break;
					
				/*case Pause:
					EZ.refreshScreen();
					pause.update();
					break;*/
					
				case EndScreen:
					endScreen.update();
					break;
					
				case Exit:
					System.exit(0);
					break;
					
				default:
					break;
			}
		}
	}
}
