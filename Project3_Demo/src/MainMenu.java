import java.util.ArrayList;
import java.util.List;

import enums.GameState;

public class MainMenu
{
	private final GameStateManager gsm;
	private final int width;
	private final int height;
	
	private final int BTN_OFFSET = 30;
	
	private EZImage startBtn;
	private EZImage exitBtn;
	private EZImage helpBtn;
	private EZImage background;
	private final List<EZElement> mainMenuElements = new ArrayList<>();
	
	private boolean shouldSetup = true;
	
	private boolean isInHelpMenu = false;
	private EZImage helpPic;
	private EZImage mainBtn;
	private List<EZElement> helpElements = new ArrayList<>();
	
	private boolean shouldSetupHelp = true;
	private boolean shouldShowHelpElements = false;
	
	public MainMenu(int _width, int _height, GameStateManager _gsm)
	{
		width = _width;
		height = _height;
		gsm = _gsm;
		
		EZ.removeAllEZElements();
	}
	
	public void update()
	{
		if (isInHelpMenu)
		{
			// Setup elements 
			if (shouldSetupHelp)
			{
				helpPic = EZ.addImage("bg_help.png", width / 2, height / 2);
				mainBtn = EZ.addImage("BtnToMainMenu.png", width / 2, height - 200);
				
				helpElements.clear();
				helpElements.add(helpPic);
				helpElements.add(mainBtn);
				
				shouldSetupHelp = false;
			}
			
			if (shouldShowHelpElements)
			{
				// Show help elements
				for (EZElement ezElement : helpElements)
				{
					ezElement.show();
				}
				
				// Hide all start menu elements to ensure
				// clicks aren't being transfered to buttons
				// in the background
				for (EZElement ezElement : mainMenuElements)
				{
					ezElement.hide();
				}
				
				shouldShowHelpElements = false;
			}
			
			if (EZInteraction.wasMouseLeftButtonPressed())
			{
				// If main menu button was clicked,
				// return back to the main menu
				if (mainBtn.isPointInElement(EZInteraction.getXMouse(), EZInteraction.getYMouse()))
				{
					// Ensure to show all main menu elements
					for (EZElement ezElement : mainMenuElements)
					{
						ezElement.show();
					}
					
					// Hide help elements
					for (EZElement ezElement : helpElements)
					{
						ezElement.hide();
					}
					
					isInHelpMenu = false;
					shouldShowHelpElements = false;
				}
			}
			
			// Return here because if we're still
			// in the help menu, there's no reason
			// to check anything else beyond this
			return;
		}
		
		if (shouldSetup)
		{
			// Clean the screen
			EZ.removeAllEZElements();
			
			background = EZ.addImage("bg_startmenu.png", width / 2, height / 2);
			startBtn = EZ.addImage("BtnStart.png", width / 2, height / 2 - BTN_OFFSET);
			helpBtn = EZ.addImage("BtnHelp.png", width / 2, height / 2 + startBtn.getHeight());
			exitBtn = EZ.addImage("BtnExit.png", width / 2, height / 2 + startBtn.getHeight() + helpBtn.getHeight() + BTN_OFFSET);
			
			mainMenuElements.clear();
			mainMenuElements.add(background);
			mainMenuElements.add(startBtn);
			mainMenuElements.add(helpBtn);
			mainMenuElements.add(exitBtn);
			
			shouldSetup = false;
		}
		
		if (EZInteraction.wasMouseLeftButtonPressed())
		{
			if (startBtn.isPointInElement(EZInteraction.getXMouse(), EZInteraction.getYMouse()))
			{
				shouldSetup = true;
				shouldSetupHelp = true;
				gsm.setState(GameState.NewGame);
			}
			
			if (helpBtn.isPointInElement(EZInteraction.getXMouse(), EZInteraction.getYMouse()))
			{
				isInHelpMenu = true;
				shouldShowHelpElements = true;
			}
			
			// If exit button pressed... quit the whole application
			if (exitBtn.isPointInElement(EZInteraction.getXMouse(), EZInteraction.getYMouse()))
			{
				gsm.setState(GameState.Exit);
			}
		}
		
		EZ.refreshScreen();
	}
}
