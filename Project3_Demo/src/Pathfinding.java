import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Pathfinding
{
	private TileSet tiles;
	private boolean isDiagAllowed;
	
	/**
	 * Creates a new Pathfinding object
	 * @param _tiles The TileSet to use for pathfinding
	 * @param _isDiagAllowed Diagonal pathing allowed or not
	 */
	public Pathfinding(TileSet _tiles, boolean _isDiagAllowed)
	{
		tiles = _tiles;
		isDiagAllowed = _isDiagAllowed;
	}
	
	/**
	 * Find a path given the start point and end point
	 * @param startPt The start Point
	 * @param endPt The end Point
	 * @return Returns a list of Cells. If a path can not
	 * be fully made to the end point, a partial path will
	 * be returned.
	 */
	public List<Cell> findPath(Point startPt, Point endPt)
	{
		List<Cell> openSet = new ArrayList<>();
		List<Cell> closeSet = new ArrayList<>();
		
		Cell startCell = tiles.getCellFromPosition(startPt);
		Cell endCell = tiles.getCellFromPosition(endPt);
		
		Cell closestCell = startCell;
		closestCell.setHCost(getDist(closestCell, endCell));

		openSet.add(startCell);
		
		while (openSet.size() > 0)
		{
			Cell currentCell = openSet.get(0);
			
			// Get cell in openSet with the lowest fCost
			for (int i = 1; i < openSet.size(); i++)
			{
				if (openSet.get(i).getFCost() < currentCell.getFCost() 
						|| (openSet.get(i).getFCost() == currentCell.getFCost() && openSet.get(i).getHCost() < currentCell.getHCost()))
				{
					currentCell = openSet.get(i);
				}
			}
			
			openSet.remove(currentCell);
			closeSet.add(currentCell);
			
			// The currentCell we're checking is the endCell
			// return the path
			if (currentCell == endCell)
			{
				return getPath(startCell, endCell);
			}
			
			// Get neighbors and recalculate gCost and hCost and set parents
			for (Cell cell : tiles.getNeighbors(currentCell, isDiagAllowed))
			{
				// Skip cell if it's not walkable or in the closedSet
				if (!cell.isWalkable() || closeSet.contains(cell))
				{
					continue;
				}
				
				// calculate new gCost
				// FIX GCOST
				int costToNeighbor = currentCell.getGCost() + getDist(currentCell, cell);
				
				// If gCost to the cell is lower or cell is not in openSet
				// change fCost of cell (we don't set an fCost value, we get it by calling getFCost()),
				// which essentially is just gCost + hCost
				if (costToNeighbor < cell.getGCost() || !openSet.contains(cell))
				{
					cell.setGCost(costToNeighbor);
					cell.setHCost(getDist(cell, endCell));
					cell.setParent(currentCell);
					
					if (!openSet.contains(cell))
					{
						openSet.add(cell);
					}
					
					if (closestCell == null || closestCell.getHCost() > cell.getHCost())
					{
						closestCell = cell;
					}
				}
			}
		}
		
		// return null if we did not find a path
		//return null;
		
		// return the closest partial path to the endcell
		return getPath(startCell, closestCell);
	}
	
	/**
	 * Calculate hCost (heuristic)
	 * @param a Cell original
	 * @param b Cell target
	 * @return Returns hCost (the heuristic value)
	 */
	private int getDist(Cell a, Cell b)
	{
		// Cost to move diagonal is 14
		// Cost to move vertically is 10
		// Get these values using a 1/1/sqrt(2) triangle and multiplying it by 10 for simplicity
		
		// ensure we don't get negative values
		int xDist = Math.abs(a.getX() - b.getX());
		int yDist = Math.abs(a.getY() - b.getY());
		
		if (!isDiagAllowed)
		{
			// All movement values have the same cost if we do not allow diagonal
			return xDist + yDist;
			//return (int)Point2D.distance(a.getX(), a.getY(), b.getX(), b.getY());
		}
		else
		{
			// Diagonal movements cost more than vertical movements
			return (xDist > yDist) ? (14 * yDist) + (10 * (xDist - yDist)) : (14 * xDist) + (10 * (yDist - xDist));
			//return (int)Point2D.distance(a.getX(), a.getY(), b.getX(), b.getY());
		}
		
	}
	
	/**
	 * Returns the path found from start cell to end cell
	 * @param startCell 
	 * @param endCell
	 * @return Returns the path found, in reverse order to correct the path from start to finish
	 */
	private List<Cell> getPath(Cell startCell, Cell endCell)
	{
		List<Cell> path = new ArrayList<>();
		
		Cell current = endCell;
		
		while(current != startCell)
		{
			path.add(current);
			current = current.getParent();
		}
		
		Collections.reverse(path);
		return path;
	}
	
}
