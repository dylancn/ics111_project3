import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import enums.ProjectileType;

public class ProjectileManager
{
	private final List<Projectile> myProjectiles;
	private final ProjectileEffectManager projectileEffectManager;
	
	public ProjectileManager()
	{
		myProjectiles = new ArrayList<>();
		projectileEffectManager = new ProjectileEffectManager();
	}
	
	/**
	 * Updates all Projectiles
	 */
	public void update()
	{
		for (Projectile current : myProjectiles)
		{
			// Have to use isFinished because this let's us
			// know when the projectile has actually finished 
			// and that it should be marked as inactive
			if (current.isFinished)
			{
				if (current.getType().hasAfterEffect())
				{
					//projectileEffectManager.activateOrMakeNextAvailable(current.getType().getEffectsType(), new Point(current.getProjectileImg().getXCenter(), current.getProjectileImg().getYCenter()));
					projectileEffectManager.activateOrMakeNextAvailable(current.getType().getEffectsType(), new Point(current.getTarget().getImage().getXCenter(), current.getTarget().getImage().getYCenter()));
				}
				
				current.makeInactive();
				continue;
			}
			
			current.update();
		}
		
		projectileEffectManager.update();
	}
	
	/**
	 * 
	 * @param projectileType The Projectile Type that should be used
	 * @param tower The tower that this projectile belongs to
	 * @param target The target that the projectile will hit
	 * @return True if a projectile was reused or made successfully, false otherwise.
	 */
	public boolean activateOrMakeNextAvailable(ProjectileType projectileType, Tower tower, Unit target)
	{
		// Active a projectile already made
		// and return
		for (Projectile projectile : myProjectiles)
		{
			if (projectile.getType().equals(projectileType))
			{
				if (!projectile.isActive())
				{
					projectile.makeActive(tower, target);
					return true;
				}
			}
		}
		
		// Make a new projectile if needed
		Projectile myProjectile = null;
		
		switch (projectileType)
		{
			case Laser:
				myProjectile = new ProjectileLaser();
				myProjectile.makeActive(tower, target);
				myProjectiles.add(myProjectile);
				break;
				
			case Missile:
				myProjectile = new ProjectileMissile();
				myProjectile.makeActive(tower, target);
				myProjectiles.add(myProjectile);
				break;
				
			case Lightning:
				myProjectile = new ProjectileLightning();
				myProjectile.makeActive(tower, target);
				myProjectiles.add(myProjectile);
				break;
				
			case Fire:
				myProjectile = new ProjectileFire();
				myProjectile.makeActive(tower, target);
				myProjectiles.add(myProjectile);
				break;
				
			case Ice:
				myProjectile = new ProjectileIce();
				myProjectile.makeActive(tower, target);
				myProjectiles.add(myProjectile);
				break;

			default:
				break;
		}
		
		if (myProjectiles.contains(myProjectile))
			return true;
		
		return false;
	}
	
	/**
	 * 
	 * @param projectileType The Projectile Type that should be used
	 * @param tower The tower that this projectile belongs to
	 * @param target The list of targets that the projectile will hit
	 * @return True if a projectile was reused or made successfully, false otherwise.
	 */
	public boolean activateOrMakeNextAvailable(ProjectileType projectileType, Tower tower, List<Unit> target)
	{
		// Active a projectile already made
		// and return
		for (Projectile projectile : myProjectiles)
		{
			if (projectile.getType().equals(projectileType))
			{
				if (!projectile.isActive())
				{
					projectile.makeActive(tower, target);
					return true;
				}
			}
		}
		
		// Make a new projectile if needed
		Projectile myProjectile = null;
		
		switch (projectileType)
		{
			case Laser:
				myProjectile = new ProjectileLaser();
				myProjectile.makeActive(tower, target);
				myProjectiles.add(myProjectile);
				break;
				
			case Missile:
				myProjectile = new ProjectileMissile();
				myProjectile.makeActive(tower, target);
				myProjectiles.add(myProjectile);
				break;
				
			case Lightning:
				myProjectile = new ProjectileLightning();
				myProjectile.makeActive(tower, target);
				myProjectiles.add(myProjectile);
				break;
				
			case Fire:
				myProjectile = new ProjectileFire();
				myProjectile.makeActive(tower, target);
				myProjectiles.add(myProjectile);
				break;
				
			case Ice:
				myProjectile = new ProjectileIce();
				myProjectile.makeActive(tower, target);
				myProjectiles.add(myProjectile);
				break;

			default:
				break;
		}
		
		if (myProjectiles.contains(myProjectile))
			return true;
		
		return false;
	}
}
