import java.awt.Color;

import enums.GameState;

public class Pause
{
	private EZText paused;
	private int width;
	private int height;
	private GameStateManager gsm;
	private int txtSize = 100;

	public Pause(int _width, int _height, GameStateManager _gsm)
	{
		width = _width;
		height = _height;
		gsm = _gsm;
	}

	public void update()
	{
		if (paused != null && !paused.isShowing)
			paused.show();
		else if (paused == null)
			paused = EZ.addText(width / 2, height / 2, "PAUSED", Color.white, txtSize);

		if (EZInteraction.wasKeyPressed('p'))
		{
			hide();
			EZ.refreshScreen();
		}
	}
	
	public void update(boolean stopPause)
	{
		if (paused != null && !paused.isShowing)
		{
			paused.show();
			paused.pullToFront();
		}
		else if (paused == null)
			paused = EZ.addText(width / 2, height / 2, "PAUSED", Color.white, txtSize);

		if (EZInteraction.wasKeyPressed('p') || stopPause)
		{
			hide();
			EZ.refreshScreen();
		}
	}

	public void hide()
	{
		if (paused != null)
			paused.hide();

		gsm.setState(GameState.Game);
	}
}
