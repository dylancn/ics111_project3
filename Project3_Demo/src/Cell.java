
import java.awt.Point;

public class Cell
{
	private boolean walkable;
	private Point position;
	
	private int gCost;
	private int hCost;
	
	private int x;
	private int y;
	
	private Cell parent;
	private final int cellRadius;
	
	private EZImage cellImage;
	private boolean hasImage = false;
	
	public Cell(boolean _walkable, Point _position, int _x, int _y, int _cellRadius)
	{
		walkable = _walkable;
		position = _position;
		x = _x;
		y = _y;
		cellRadius = _cellRadius;
	}
	
	public Point getIndex()
	{
		return new Point(x, y);
	}
	
	public void setCellImg(String img, boolean hideImage)
	{
		hasImage = true;
		cellImage = EZ.addImage(img, getCenterPoint().x, getCenterPoint().y);
		
		if (hideImage)
		{
			cellImage.hide();
		}
	}
	
	public void hideCellImg()
	{
		if (hasCellImg())
		{
			cellImage.hide();
		}
	}
	
	public void showCellImg()
	{
		if (hasCellImg())
		{
			cellImage.show();
		}
	}
	
	public boolean hasCellImg()
	{
		return hasImage;
	}
	
	public void removeCellImg()
	{
		if (cellImage != null)
		{
			EZ.removeEZElement(cellImage);
			cellImage = null;
			hasImage = false;
		}
	}

	public void setParent(Cell _parent)
	{
		parent = _parent;
	}
	
	public Cell getParent()
	{
		return parent;
	}
	
	public Point getCenterPoint()
	{
		// grid is split so the top left corner is the start
		// however, EZ places everything in the middle
		// so return position + radius of cell
		return new Point(position.x + cellRadius, position.y + cellRadius);
	}
	
	public Point getTopLeftPoint()
	{
		// grid is split so the top left corner is the start
		// however, EZ places everything in the middle
		// so return position + radius of cell
		return new Point(position.x, position.y);
	}
	
	public int getFCost()
	{
		return gCost + hCost;
	}
	
	public int getHCost()
	{
		return hCost;
	}
	
	public int getGCost()
	{
		return gCost;
	}
	
	public void setGCost(int _gCost)
	{
		gCost = _gCost;
	}
	
	public void setHCost(int _hCost)
	{
		hCost = _hCost;
	}
	
	public int getX()
	{
		return x;
	}
	
	public int getY()
	{
		return y;
	}
	
	public boolean isWalkable()
	{
		return walkable;
	}
	
	public void setWalkable(boolean _walkable)
	{
		walkable = _walkable;
	}
}
