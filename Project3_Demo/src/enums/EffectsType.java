package enums;

public enum EffectsType
{
	MissileExplosion("PE_Explosion128.png", 32, 32, 4, 5),
	FireballExplosion("PE_FireballExplosion32.png", 32, 32, 9, 1),
	IceballExplosion("PE_IceballExplosion.png", 48, 48, 6, 5);
	
	private final String imgName;
	private final int singleWidth;
	private final int singleHeight;
	private final int rows;
	private final int columns;
	private final int finishCounter;
	private final boolean singleColumn;
	
	private EffectsType(String fullImgName, int _singleWidth, int _singleHeight, int _rows, int _columns)
	{
		imgName = fullImgName;
		singleWidth = _singleWidth;
		singleHeight = _singleHeight;
		rows = _rows;
		columns = _columns;
		finishCounter = rows * columns;
		
		if (_columns > 1)
			singleColumn = false;
		else
			singleColumn = true;
	}
	
	public String getImgName()
	{
		return imgName;
	}
	
	public int getSingleWidth()
	{
		return singleWidth;
	}
	
	public int getSingleHeight()
	{
		return singleHeight;
	}
	
	public int getRows()
	{
		return rows;
	}
	
	public int getColumns()
	{
		return columns;
	}
	
	public int getFinishCounter()
	{
		return finishCounter;
	}
	
	public boolean isSingleColumn()
	{
		return singleColumn;
	}
}
