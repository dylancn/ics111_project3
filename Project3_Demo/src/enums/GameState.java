package enums;

public enum GameState
{
	NewGame,
	MainMenu,
	Game,
	Pause,
	EndScreen,
	Exit,
}
