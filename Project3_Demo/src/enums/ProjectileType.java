package enums;

public enum ProjectileType
{
	Laser(100000, null, null),
	Missile(70, EffectsType.MissileExplosion, ElementalType.Physical),
	Lightning(100000, null, ElementalType.Lightning),
	Fire(100, EffectsType.FireballExplosion, ElementalType.Fire),
	Ice(100, EffectsType.IceballExplosion, ElementalType.Ice);
	
	int travelSpeed;
	final EffectsType effectsType;
	final boolean hasAfterEffect;
	final ElementalType elementalType;
	
	private ProjectileType(int _speed, EffectsType _effectsType, ElementalType _elementalType)
	{
		travelSpeed = _speed;

		if (_effectsType != null)
		{
			effectsType = _effectsType;
			hasAfterEffect = true;
		}
		else
		{
			effectsType = null;
			hasAfterEffect = false;
		}
		
		elementalType = _elementalType;
	}
	
	public int getSpeed()
	{
		return travelSpeed;
	}
	
	public EffectsType getEffectsType()
	{
		return effectsType;
	}
	
	public boolean hasAfterEffect()
	{
		return hasAfterEffect;
	}
	
	public ElementalType getElementalType()
	{
		return elementalType;
	}
}
