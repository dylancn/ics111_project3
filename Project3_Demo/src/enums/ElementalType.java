package enums;

public enum ElementalType
{
	Physical,
	Lightning,
	Fire,
	Ice;
}
