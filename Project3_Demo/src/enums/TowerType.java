package enums;

public enum TowerType
{
	Laser(ProjectileType.Laser, "turretLaser.png", 1000, 1, 10f),
	Missile(ProjectileType.Missile, "turret.png", 1000, 3, 15.0f),
	Lightning(ProjectileType.Lightning,"turretLightning.png", 1000, 2, 10.0f),
	Fire(ProjectileType.Fire, "turretFire.png", 1000, 2, 7.0f),
	Ice(ProjectileType.Ice, "turretIce.png", 1000, 2, 7.0f);
	
	private final ProjectileType projectileType;
	private final String imageString;
	private int attackSpeed;
	private int attackRange;
	private float baseDamage;
	
	private TowerType(ProjectileType _projectileType, String _imageString, int _attackSpeed, int _attackRange, float _baseDamage)
	{
		projectileType = _projectileType;
		
		imageString = _imageString;
		
		attackSpeed = _attackSpeed;
		attackRange = _attackRange;
		baseDamage = _baseDamage;
	}
	
	public int getAtkSpd()
	{
		return attackSpeed;
	}
	
	/**
	 * 
	 * @return The amount of cells that this tower
	 * can reach.
	 */
	public int getAtkRng()
	{
		return attackRange;
	}
	
	public float getBaseDamange()
	{
		return baseDamage;
	}
	
	public ProjectileType getProjectileType()
	{
		return projectileType;
	}
	
	public String getImageString()
	{
		return imageString;
	}
	
	@Override
	public String toString()
	{
		return projectileType + " Tower";
	}
}
