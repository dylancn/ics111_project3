package enums;

public enum UnitType
{
	/*
	 * Floats less than 1 decrease that amount of damage
	 * Floats equal to 1 deal regular damage
	 * Floats larger than 1 deal increased damage
	 * 
	 * Humans are neutral. Health = Base * 1.0
	 * 
	 * Orcs are slower, but more beefy. Health = Base * 1.25;
	 * 
	 * Elves. Health = Base * 1.05
	 * 
	 * Base health starts at 100
	 */
	Human_Heavy(0.9f, 1.0f, 1.0f, 1.0f, (int)(100 * 1.0), 25),
	Human_Light(1.0f, 1.0f, 1.0f, 1.0f, (int)(100 * 1.0), 30),
	Orc_Heavy(0.80f, 1.1f, 1.1f, 1.1f, (int)(100 * 1.25), 21),
	Orc_Light(0.90f, 1.1f, 1.1f, 1.1f, (int)(100 * 1.25), 25),
	Elf_Light(1.1f, 0.9f, 0.9f, 0.9f, (int)(100 * 1.05), 30),
	Dragon(1.0f, 1.0f, 1.0f, 1.0f, (int)(100 * 1.0), 25),
	Blue_Dragon(0.9f, 1.0f, 1.0f, 1.0f, (int)(100 * 1.0), 25),
	Black_Dragon(0.9f, 1.0f, 1.0f, 1.0f, (int)(100 * 1.0), 25),
	Vampire(0.9f, 1.0f, 1.0f, 1.0f, (int)(100 * 1.0), 25),
	Bat(0.9f, 1.0f, 1.0f, 1.0f, (int)(100 * 1.0), 25),
	Bot(0.9f, 1.0f, 1.0f, 1.0f, (int)(100 * 1.0), 25),
	Boss(0.9f, 1.0f, 1.0f, 1.0f, (int)(100 * 1.0), 25);
	
	float physicalResist;
	float lightningResist;
	float fireResist;
	float iceResist;
	int health;
	int movementSpeed;
	
	private UnitType(float _physicalResist, float _lightningResist, float _fireResist, float _iceResist, int _health, int _movementSpeed)
	{
		physicalResist = _physicalResist;
		lightningResist = _lightningResist;
		fireResist = _fireResist;
		iceResist = _iceResist;
		health = _health;
		movementSpeed = _movementSpeed;
	}
	
	public float getPhysicalResist()
	{
		return physicalResist;
	}
	
	public float getLightningResist()
	{
		return lightningResist;
	}
	
	public float getFireResist()
	{
		return fireResist;
	}
	
	public float getIceResist()
	{
		return iceResist;
	}
	
	public int getHealth()
	{
		return health;
	}
	
	public int getMovementSpeed()
	{
		return movementSpeed;
	}
}
