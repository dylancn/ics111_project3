import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import enums.TowerType;
import helper.DeltaTime;
import helper.MathHelper;

public class Tower
{
	private final TowerType towerType;
	private final EZImage towerImage;
	private float attackDelay;
	private float damage;
	private final float baseDamage;
	private float damageMultiplier;
	private List<Cell> attackableCells;
	private List<Unit> units;
	private final ProjectileManager projectileManager;
	//private int attackRange;
	private double rotation;
	private Unit target;
	private int timeTotal = 0;
	
	public Tower(TowerType _type, Point pt, List<Cell> _attackableCells, List<Unit> _units, ProjectileManager _projectileManager)
	{
		towerType = _type;
		towerImage = EZ.addImage(towerType.getImageString(), pt.x, pt.y);
		//attackRange = towerType.getAtkRng();
		
		attackDelay = towerType.getAtkSpd();
		baseDamage = towerType.getBaseDamange();
		damage = towerType.getBaseDamange();
		damageMultiplier = 1.0f;
		
		attackableCells = _attackableCells;
		units = _units;
		projectileManager = _projectileManager;
	}
	
	public EZImage getImage()
	{
		return towerImage;
	}
	
	public Point getLocation()
	{
		return new Point(towerImage.getXCenter(), towerImage.getYCenter());
	}
	
	private Unit acquireTargetAdv()
	{
		Unit target = null;
		int targetIndex = -1;
		float targetHp = 0;
		
		if (units != null && !units.isEmpty())
		{
			List<Unit> validTargets = new ArrayList<>();
			
			for (int i = 0; i < units.size(); i++)
			{
				Unit currentUnit = units.get(i);
				
				if (attackableCells.contains(currentUnit.getUnitCellLoc()))
				{
					validTargets.add(currentUnit);
				}
			}
			
			if (validTargets.size() > 0)
			{
				if (towerType.equals(TowerType.Ice))
				{
					for (Unit unit : validTargets)
					{
						// Find the first target without
						// max ice stacks and target them
						if (!unit.hasMaxIceStacks())
						{
							target = unit;
							break;
						}
						else
						{
							if (unit.getPredictiveHp() > 0 || unit.getDistanceToEnd() < 5)
							{
								int tempIndex = unit.getIndexOfCurrentPathCell();
								float tempHp = unit.getHp();
								
								if (target == null || targetIndex == -1)
								{
									target = unit;
									targetIndex = unit.getIndexOfCurrentPathCell();
									targetHp = unit.getHp();
								}
								else if (targetIndex == tempIndex)
								{
									if (targetHp > tempHp)
									{
										target = unit;
										targetIndex = tempIndex;
										targetHp = tempHp;
									}
								}
								else if (targetIndex < tempIndex)
								{
									target = unit;
									targetIndex = tempIndex;
									targetHp = tempHp;
								}
							}
						}
					}
					
					if (target == null)
					{
						return validTargets.get(0);
					}
					
					return target;
				}
				else
				{
					// THIS LOOP NEEDS TO BE CHANGED
					for (int i = 0; i < units.size(); i++)
					{
						Unit currentUnit = units.get(i);
								
						if (attackableCells.contains(currentUnit.getUnitCellLoc()))
						{
							// Smart Targeting for everything else
							
							// Only include targets if their predicted health
							// is still greater than 0 OR the amount of tiles
							// they have until they reach the end is less than 5.
							// This will force all available towers to focus fire
							// on imminent threats and to avoid potentially firing
							// on a target that will otherwise die to incoming damage
							// from other towers.
							
							// In the case that the index of the units are the same,
							// choose the target with the lowest amount of hp remaining
							if (currentUnit.getPredictiveHp() > 0 || currentUnit.getDistanceToEnd() < 5)
							{
								int tempIndex = currentUnit.getIndexOfCurrentPathCell();
								float tempHp = currentUnit.getHp();
								
								if (target == null || targetIndex == -1)
								{
									target = currentUnit;
									targetIndex = currentUnit.getIndexOfCurrentPathCell();
									targetHp = currentUnit.getHp();
								}
								else if (targetIndex == tempIndex)
								{
									if (targetHp > tempHp)
									{
										target = currentUnit;
										targetIndex = tempIndex;
										targetHp = tempHp;
									}
								}
								else if (targetIndex < tempIndex)
								{
									target = currentUnit;
									targetIndex = tempIndex;
									targetHp = tempHp;
								}
							}
						}
					}
					
					if (target == null)
						return validTargets.get(0);
					
					return target;
				}
			}
		}
		
		return null;
	}
	
	private Unit acquireTarget()
	{
		if (units != null && !units.isEmpty())
		{
			// Get a list of valid units
			/*List<Unit> validTargets = new ArrayList<>();
			for (Unit unit : units)
			{
				if (Point2D.distance(towerImage.getXCenter(), towerImage.getYCenter(), unit.getImage().getXCenter(), unit.getImage().getYCenter()) < attackRange)
				{
					validTargets.add(unit);
				}
			}
			
			// Get the closest unit for attack
			if (validTargets.size() > 0)
			{
				return validTargets.stream().sorted((e1, e2) -> Double.compare(
							Point2D.distance(towerImage.getXCenter(), towerImage.getYCenter(), e1.getImage().getXCenter(), e1.getImage().getYCenter()), 
							Point2D.distance(towerImage.getXCenter(), towerImage.getYCenter(), e2.getImage().getXCenter(), e2.getImage().getYCenter()))).findFirst().get();
			}*/
			
			// Just return the first unit that we can attack,
			// unless a more advanced targeting system is needed.
			// Due to units now having different speeds, the front
			// most unit will be the one with the highest path index
			Unit target = null;
			int targetIndex = -1;
			float targetHp = 0;
			for (int i = 0; i < units.size(); i++)
			{
				Unit currentUnit = units.get(i);
						
				if (attackableCells.contains(currentUnit.getUnitCellLoc()))
				{
					// Smart Targeting for everything else
					
					// Only include targets if their predicted health
					// is still greater than 0 OR the amount of tiles
					// they have until they reach the end is less than 5.
					// This will force all available towers to focus fire
					// on imminent threats and to avoid potentially firing
					// on a target that will otherwise die to incoming damage
					// from other towers.
					
					// In the case that the index of the units are the same,
					// choose the target with the lowest amount of hp remaining
					if (currentUnit.getPredictiveHp() > 0 || currentUnit.getDistanceToEnd() < 5)
					{
						int tempIndex = currentUnit.getIndexOfCurrentPathCell();
						float tempHp = currentUnit.getHp();
						
						if (target == null || targetIndex == -1)
						{
							target = currentUnit;
							targetIndex = currentUnit.getIndexOfCurrentPathCell();
							targetHp = currentUnit.getHp();
						}
						else if (targetIndex == tempIndex)
						{
							if (targetHp > tempHp)
							{
								target = currentUnit;
								targetIndex = tempIndex;
								targetHp = tempHp;
							}
						}
						else if (targetIndex < tempIndex)
						{
							target = currentUnit;
							targetIndex = tempIndex;
							targetHp = tempHp;
						}
					}
				}
			}
			
			return target;
		}
		
		return null;
	}
	
	private ArrayList<Unit> acquireLightningTargets()
	{
		if (units != null && !units.isEmpty())
		{
			ArrayList<Unit> targets = new ArrayList<>();
			
			// Add all targets in attack range
			for (int i = 0; i < units.size(); i++)
			{
				Unit currentUnit = units.get(i);
						
				if (attackableCells.contains(currentUnit.getUnitCellLoc()))
				{
					if ((currentUnit.getPredictiveHp() > 0 || currentUnit.getDistanceToEnd() < 5) && !currentUnit.isDead())
					{
						targets.add(currentUnit);
					}
				}
			}
			
			if (targets.size() > 0)
			{
				// Sort the targets based on their distance
				// to the end first... this gives us the
				// farthest unit
				targets.sort((u1, u2) -> Integer.compare(u1.getDistanceToEnd(), u2.getDistanceToEnd()));
				
				// Then sort by their distance to the first
				// unit in the list
				Unit mainTarget = targets.get(0);
				targets.remove(mainTarget);
				
				targets.sort((u1, u2) -> Double.compare(
						MathHelper.getDistance(towerImage.getXCenter(), towerImage.getYCenter(), u1.getImage().getXCenter(), u1.getImage().getYCenter()), 
						MathHelper.getDistance(towerImage.getXCenter(), towerImage.getYCenter(), u2.getImage().getXCenter(), u2.getImage().getYCenter())));
				
				// Add main target back to the list
				targets.add(0, mainTarget);
				
				// This ensures there's up to 5 units in the
				// list and we remove from the back first,
				// to ensure we get the 5 farthest units
				// along the path
				if (targets.size() > 5)
				{
					for (int i = targets.size() - 1; i >= 0; i--)
					{
						if (targets.size() > 5)
						{
							targets.remove(i);
						}
						else
						{
							break;
						}
					}
				}
				
				return targets;
			}
		}
		
		return null;
	}
	
	private void attack()
	{
		if (timeTotal >= attackDelay)
		{
			// A very crude method to allow the
			// lightning tower to cause damage to
			// multiple targets. This will suffice
			// unless there's time to make a more
			// elaborate system for controlling
			// multiple unit target
			if (towerType.equals(TowerType.Lightning))
			{
				List<Unit> targetUnits = acquireLightningTargets();
				
				if (targetUnits != null && !targetUnits.isEmpty())
				{
					// We're going to actually shoot
					// at the farthest unit and then
					// chain the damage down the list.
					// So rotate to that unit
					rotation = MathHelper.calcRotationAngleInDegrees(new Point(towerImage.getXCenter(), towerImage.getYCenter()), new Point(targetUnits.get(0).getImage().getXCenter(), targetUnits.get(0).getImage().getYCenter()));
					towerImage.rotateTo(rotation);
					
					// Make a projectile active with
					// the list of units
					projectileManager.activateOrMakeNextAvailable(towerType.getProjectileType(), Tower.this, targetUnits);
					
					// Reset time
					timeTotal = 0;
				}
				
				return;
			}
			
			// Single Targeting for everything
			// else
			target = acquireTargetAdv();
			
			if (target != null && !target.isDead())
			{
				// Rotate the turret to face target
				rotation = MathHelper.calcRotationAngleInDegrees(new Point(towerImage.getXCenter(), towerImage.getYCenter()), new Point(target.getImage().getXCenter(), target.getImage().getYCenter()));
				towerImage.rotateTo(rotation);
				
				// Get a projectile to use
				projectileManager.activateOrMakeNextAvailable(towerType.getProjectileType(), Tower.this, target);
				
				// Reset time
				timeTotal = 0;
			}
		}
		else
		{
			timeTotal += DeltaTime.getDeltaTime();
		}
	}
	
	public void update()
	{
		attack();
	}
	
	public void upgrade()
	{
		// This probably needs to change based on each tower.
		// For now just keep a value so we know that it's
		// working and to test
		damageMultiplier += 0.1;
		damage = baseDamage * damageMultiplier;
	}
	
	public float getDamage()
	{
		return damage;
	}
	
	public TowerType getTowerType()
	{
		return towerType;
	}
	
	public void destroy()
	{
		EZ.removeEZElement(towerImage);
	}
	
	@Override
	public String toString()
	{
		return towerType.toString();
	}
}
