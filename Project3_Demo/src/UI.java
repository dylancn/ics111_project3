import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import enums.GameState;
import enums.TowerType;
import helper.DeltaTime;
import helper.Pair;

public class UI
{
	private final int uiWidth;
	private final int uiHeight;
	private final int gameWidth;
	private final GameStateManager gsm;
	private final ErrorMessages errorMessages;
	private final TowerManager towerManager;
	private final Pause pause;
	@SuppressWarnings("unused")
	private final EZImage uiBG;
	private final EZImage uiRight;
	private final EZImage uiLeft;
	private final EZImage uiSpeedInc;
	private final EZImage uiSpeedDec;
	private final EZImage uiPause;
	private final EZImage uiPlay;
	private final EZImage uiBuy;
	
	private final List<Pair<TowerType, EZGroup>> towerGroups = new ArrayList<>();
	private int groupIndex = 0;
	
	public UI(int _width, int _height, GameStateManager _gsm, ErrorMessages _errorMessages, TowerManager _towerManager)
	{
		uiWidth = _width;
		uiHeight = _height;
		gsm = _gsm;
		errorMessages = _errorMessages;
		towerManager = _towerManager;
		
		pause = new Pause(_width, _height, _gsm);
		gameWidth = uiWidth - 200;
		
		uiBG = EZ.addImage("bg_ui.png", uiWidth - 100, uiHeight / 2);
		
		uiRight = EZ.addImage("BtnNext.png", -100, -100);
		uiLeft = EZ.addImage("BtnNext.png", -100, -100);
		
		uiSpeedInc = EZ.addImage("BtnSpeed.png", -100, -100);
		uiSpeedDec = EZ.addImage("BtnSpeed.png", -100, -100);
		
		uiPause = EZ.addImage("BtnPause.png", -100, -100);
		uiPlay = EZ.addImage("BtnPlay.png", -100, -100);
		
		uiBuy = EZ.addImage("BtnBuy.png", -100, -100);
		
		uiRight.scaleTo(0.5);
		uiLeft.scaleTo(0.5);
		uiPause.scaleTo(0.5);
		uiPlay.scaleTo(0.5);
		uiSpeedDec.scaleTo(0.5);
		uiSpeedInc.scaleTo(0.5);
		uiBuy.scaleTo(0.5);
		
		uiLeft.translateTo(gameWidth + (float)(200 / 3 * 0) + uiLeft.getWorldWidth(), 400);
		
		uiRight.rotateTo(180);
		uiRight.translateTo(gameWidth + (float)(200 / 3) * 2 + uiRight.getWorldWidth(), 400);
		
		uiSpeedDec.rotateTo(180);
		uiSpeedDec.translateTo(gameWidth + (float)(200 / 3) * 0 + uiSpeedDec.getWorldWidth(), 750);
		uiPause.translateTo(gameWidth + (float)(200 / 3) * 1 + uiPause.getWorldWidth(), 750);
		uiPlay.hide();
		uiPlay.translateTo(gameWidth + (float)(200 / 3) * 1 + uiPlay.getWorldWidth(), 750);
		uiSpeedInc.translateTo(gameWidth + (float)(200 / 3) * 2 + uiSpeedInc.getWorldWidth(), 750);
		
		uiBuy.translateTo(gameWidth + 100, 600);
		
		setupTowerGroups();
		towerGroups.forEach(i -> i.getSecond().pullToFront());
	}
	
	public void update()
	{
		boolean wasMouseClickInButton = false;
		
		if (EZInteraction.wasMouseLeftButtonPressed())
		{
			int x = EZInteraction.getXMouse();
			int y = EZInteraction.getYMouse();
			
			// If the game is in the paused state,
			// only check this and return
			if (gsm.getState().equals(GameState.Pause))
			{
				if (uiPlay.isShowing() && uiPlay.isPointInElement(x, y))
				{
					uiPause.show();
					uiPlay.hide();
					pause.update(true);
					
					return;
				}
				
				return;
			}
			
			// Don't allow clicks to register with any
			// button if the player is trying to place
			// a tower
			if (!towerManager.isInTowerPrePlacement())
			{
				if (uiPause.isShowing() && uiPause.isPointInElement(x, y))
				{
					gsm.setState(GameState.Pause);
					uiPlay.show();
					uiPause.hide();
					pause.update(false);
					
					wasMouseClickInButton = true;
				}
				else if (uiSpeedDec.isPointInElement(x, y))
				{
					if (DeltaTime.decreaseMultiplier(0.5f))
					{
						errorMessages.add("Game speed has been decrease!");
					}
					else
					{
						errorMessages.add("Game speed cannot be decreased further!");
					}
					
					wasMouseClickInButton = true;
				}
				else if (uiSpeedInc.isPointInElement(x, y))
				{
					if (DeltaTime.increaseMultiplier(0.5f))
					{
						errorMessages.add("Game speed has been increased!");
					}
					else
					{
						errorMessages.add("Game speed cannot be increased further!");
					}
					
					wasMouseClickInButton = true;
				}
				else if (uiRight.isPointInElement(x, y))
				{
					int temp = groupIndex + 1;
					
					if (temp > towerGroups.size() - 1)
					{
						temp = 0;
					}
					
					groupIndex = temp;
					
					for (int i = 0; i < towerGroups.size(); i++)
					{
						if (i == groupIndex)
							towerGroups.get(i).getSecond().show();
						else
							towerGroups.get(i).getSecond().hide();
					}
					
					wasMouseClickInButton = true;
				}
				else if (uiLeft.isPointInElement(x, y))
				{
					int temp = groupIndex - 1;
					
					if (temp < 0)
					{
						temp = towerGroups.size() - 1;
					}
					
					groupIndex = temp;
					
					for (int i = 0; i < towerGroups.size(); i++)
					{
						if (i == groupIndex)
							towerGroups.get(i).getSecond().show();
						else
							towerGroups.get(i).getSecond().hide();
					}
					
					wasMouseClickInButton = true;
				}
				else if (uiBuy.isPointInElement(x, y))
				{
					towerManager.towerPrePlacement(towerGroups.get(groupIndex).getFirst());
					
					wasMouseClickInButton = true;
				}
			}
		}
		
		// This can be enabled if keyboard shortcuts need
		// to be added for tower adding, so the player
		// doesn't have to always click the buy button
		// to place a tower
		/*if (!towerManager.isInTowerPrePlacement())
		{
			if (EZInteraction.wasKeyPressed('q'))
			{
				towerManager.towerPrePlacement(TowerType.Lightning);
			}
			else if (EZInteraction.wasKeyPressed('w'))
			{
				towerManager.towerPrePlacement(TowerType.Missile);
			}
		}*/
		
		towerManager.updateTowerInteraction(wasMouseClickInButton);
	}
	
	private final EZGroup missileGroup = EZ.addGroup();
	private final EZGroup lightningGroup = EZ.addGroup();
	private final EZGroup fireGroup = EZ.addGroup();
	private final EZGroup iceGroup = EZ.addGroup();
	private void setupTowerGroups()
	{
		// Missile Tower
		EZImage missileTowerImage = EZ.addImage("turret.png", -500, -500);
		missileTowerImage.translateTo(gameWidth + (float)(200 / 3 * 1) + missileTowerImage.getWorldWidth(), 400);
		
		EZText missileTowerName =  EZ.addText(gameWidth + 100, 450, "Missile Tower", Color.WHITE, 20);
		EZText missileTowerDmgType = EZ.addText(gameWidth + 100, 480, "Type: " + TowerType.Missile.getProjectileType().getElementalType().toString(), Color.WHITE, 15);
		EZText missileTowerDmg = EZ.addText(gameWidth + 100, 510, "Damage: " + TowerType.Missile.getBaseDamange(), Color.WHITE, 15);
		EZText missileTowerRng = EZ.addText(gameWidth + 100, 540, "Range: " + TowerType.Missile.getAtkRng(), Color.WHITE, 15);
		
		missileGroup.addElement(missileTowerImage);
		missileGroup.addElement(missileTowerName);
		missileGroup.addElement(missileTowerDmgType);
		missileGroup.addElement(missileTowerDmg);
		missileGroup.addElement(missileTowerRng);
		
		towerGroups.add(new Pair<TowerType, EZGroup>(TowerType.Missile, missileGroup));
		
		// Lightning Tower
		EZImage lightningTowerImage = EZ.addImage("turretLightning.png", -500, -500);
		lightningTowerImage.translateTo(gameWidth + (float)(200 / 3 * 1) + lightningTowerImage.getWorldWidth(), 400);
		
		EZText lightningTowerName =  EZ.addText(gameWidth + 100, 450, "Lightning Tower", Color.WHITE, 20);
		EZText lightningTowerDmgType = EZ.addText(gameWidth + 100, 480, "Type: " + TowerType.Lightning.getProjectileType().getElementalType().toString(), Color.WHITE, 15);
		EZText lightningTowerDmg = EZ.addText(gameWidth + 100, 510, "Damage: " + TowerType.Lightning.getBaseDamange(), Color.WHITE, 15);
		EZText lightningTowerRng = EZ.addText(gameWidth + 100, 540, "Range: " + TowerType.Lightning.getAtkRng(), Color.WHITE, 15);
		
		lightningGroup.addElement(lightningTowerImage);
		lightningGroup.addElement(lightningTowerName);
		lightningGroup.addElement(lightningTowerDmgType);
		lightningGroup.addElement(lightningTowerDmg);
		lightningGroup.addElement(lightningTowerRng);
		lightningGroup.hide();
		
		towerGroups.add(new Pair<TowerType, EZGroup>(TowerType.Lightning, lightningGroup));
		
		// Fire Tower
		EZImage	fireTowerImage = EZ.addImage("turretFire.png", -500, -500);
		fireTowerImage.translateTo(gameWidth + (float)(200 / 3 * 1) + fireTowerImage.getWorldWidth(), 400);
		
		EZText fireTowerName =  EZ.addText(gameWidth + 100, 450, "Fire Tower", Color.WHITE, 20);
		EZText fireTowerDmgType = EZ.addText(gameWidth + 100, 480, "Type: " + TowerType.Fire.getProjectileType().getElementalType().toString(), Color.WHITE, 15);
		EZText fireTowerDmg = EZ.addText(gameWidth + 100, 510, "Damage: " + TowerType.Fire.getBaseDamange(), Color.WHITE, 15);
		EZText fireTowerRng = EZ.addText(gameWidth + 100, 540, "Range: " + TowerType.Fire.getAtkRng(), Color.WHITE, 15);
		
		fireGroup.addElement(fireTowerImage);
		fireGroup.addElement(fireTowerName);
		fireGroup.addElement(fireTowerDmgType);
		fireGroup.addElement(fireTowerDmg);
		fireGroup.addElement(fireTowerRng);
		fireGroup.hide();
		
		towerGroups.add(new Pair<TowerType, EZGroup>(TowerType.Fire, fireGroup));
		
		// Ice Tower
		EZImage	iceTowerImage = EZ.addImage("turretIce.png", -500, -500);
		iceTowerImage.translateTo(gameWidth + (float)(200 / 3 * 1) + iceTowerImage.getWorldWidth(), 400);
		
		EZText iceTowerName =  EZ.addText(gameWidth + 100, 450, "Ice Tower", Color.WHITE, 20);
		EZText iceTowerDmgType = EZ.addText(gameWidth + 100, 480, "Type: " + TowerType.Ice.getProjectileType().getElementalType().toString(), Color.WHITE, 15);
		EZText iceTowerDmg = EZ.addText(gameWidth + 100, 510, "Damage: " + TowerType.Ice.getBaseDamange(), Color.WHITE, 15);
		EZText iceTowerRng = EZ.addText(gameWidth + 100, 540, "Range: " + TowerType.Ice.getAtkRng(), Color.WHITE, 15);
		
		iceGroup.addElement(iceTowerImage);
		iceGroup.addElement(iceTowerName);
		iceGroup.addElement(iceTowerDmgType);
		iceGroup.addElement(iceTowerDmg);
		iceGroup.addElement(iceTowerRng);
		iceGroup.hide();
		
		towerGroups.add(new Pair<TowerType, EZGroup>(TowerType.Ice, iceGroup));
	}
}
