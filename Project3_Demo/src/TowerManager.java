import java.awt.Color;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import enums.TowerType;
import helper.Logger;

public class TowerManager
{
	private final List<Tower> towers = new ArrayList<>();
	
	private final Logger logger = new Logger("TowerManager.java");
	
	private final int width;
	private final int height;
	private final int cellSize;
	private final TileSet tileSet;
	private final Pathfinding pathfinding;
	private final ErrorMessages errorMessages;
	private final List<Unit> unitList;
	private final ProjectileManager projectileManager;
	private boolean shouldUpdatePath = false;
	
	private boolean isTowerRangeRectangleSetup;
	public TowerManager(int _width, int _height, int _cellSize, TileSet _tileSet, Pathfinding _pathfinding, ErrorMessages _errorMessages, List<Unit> _unitList)
	{
		width = _width;
		height = _height;
		cellSize = _cellSize;
		tileSet = _tileSet;
		pathfinding = _pathfinding;
		errorMessages = _errorMessages;
		unitList = _unitList;
		projectileManager = new ProjectileManager();
		
		logger.setAllowDuplicate(true);
		
		isTowerRangeRectangleSetup = false;
	}
	
	public void update()
	{
		handleTowerAttack();
		projectileManager.update();
	}
	
	public boolean shouldUpdatePath()
	{
		if (shouldUpdatePath)
		{
			shouldUpdatePath = false;
			return true;
		}
		else
			return false;
	}
	
	/**
	 * Updates anything that involves interaction with the towers
	 */
	public void updateTowerInteraction(boolean checkTowerMenu)
	{
		// If we click any button while the tower info / upgrade
		// menu is open, then clear the tower info menu
		if (checkTowerMenu)
		{
			clearUpgradeMenu();
		}
		
		if (isInTowerPrePlacement)
		{
			// Left click places the tower
			if (EZInteraction.wasMouseLeftButtonPressed())
			{
				int x = EZInteraction.getXMouse();
				int y = EZInteraction.getYMouse();
				
				// Ensure we can create the tower,
				// if not then return and spit out
				// an error message to the user
				if (!createTower(towerPreType, x, y))
				{
					return;
				}
				
				EZ.removeEZElement(towerPreImage);
				towerPreGroup.hide();
				
				towerPreType = null;
				towerPreImage = null;
				towerPreGroup = null;
				
				isInTowerPrePlacement = false;
			}
			// Right click OR ESCAPE cancels tower placement
			else if (EZInteraction.wasMouseRightButtonPressed() || EZInteraction.wasKeyPressed(KeyEvent.VK_ESCAPE))
			{
				EZ.removeEZElement(towerPreImage);
				towerPreGroup.hide();
				
				towerPreType = null;
				towerPreImage = null;
				towerPreGroup = null;
				
				isInTowerPrePlacement = false;
			}
			// Else keep updating the positions
			else 
			{
				int x = EZInteraction.getXMouse();
				int y = EZInteraction.getYMouse();
				
				towerPreImage.translateTo(x, y);
				towerPreGroup.translateTo(x, y);
			}
			
			return;
		}
		
		// Keep the right click on a tower
		// as the remove condition for now.
		// Don't allow the removal of towers
		// while in the tower upgrade meny
		if (!isTowerMenuOpen && EZInteraction.wasMouseRightButtonPressed())
		{
			int x = EZInteraction.getXMouse();
			int y = EZInteraction.getYMouse();
			for (Tower tower : towers)
			{
				if (tower.getImage().isPointInElement(x, y))
				{
					tower.destroy();
					towers.remove(tower);
					
					// If we remove a tower, remember to make it walkable
					// Then make sure to update the path
					tileSet.getCellFromPosition(x, y).setWalkable(true);
					shouldUpdatePath = true;
					
					break;
				}
			}
		}
		
		handleTowerUpgrades();
	}
	
	/**
	 * Handles tower attacking
	 */
	private void handleTowerAttack()
	{
		for (Tower tower : towers)
		{
			//tower.update(unitList);
			tower.update();
		}
	}
	
	private boolean isInTowerPrePlacement = false;
	
	public boolean isInTowerPrePlacement()
	{
		return isInTowerPrePlacement;
	}
	
	private TowerType towerPreType = null;
	private EZImage towerPreImage = null;
	
	private EZGroup towerPreGroup = null;
	private EZGroup towerThreeTile = EZ.addGroup();
	private EZGroup towerTwoTile = EZ.addGroup();
	
	/**
	 * This creates an icon that the player can drag to visualize where they want to place the tower
	 * @param _towerType The TowerType
	 */
	public void towerPrePlacement(TowerType _towerType)
	{
		isInTowerPrePlacement = true;
		
		towerPreType = _towerType;
		towerPreImage = EZ.addImage(_towerType.getImageString(), EZInteraction.getXMouse(), EZInteraction.getYMouse());
		
		switch (_towerType.getAtkRng())
		{
			case 2:
				towerPreGroup = towerTwoTile;
				break;
				
			case 3:
				towerPreGroup = towerThreeTile;
				break;

			default:
				break;
		}
		
		towerPreGroup.pullToFront();
		towerPreGroup.show();
	}
	
	/**
	 * Creates a tower
	 * @param type The TowerType to create
	 * @param mouseX The x location
	 * @param mouseY The y location
	 * @return True if tower was successfully created, false otherwise
	 */
	private boolean createTower(TowerType type, int mouseX, int mouseY)
	{
		// Check to ensure that mouse position is within
		// the screen to avoid getting an out of bounds
		// exception caused by passing in bad positions to
		// get the cell
		if (mouseX > 0 && mouseX <= width && mouseY > 0 && mouseY <= height)
		{
			Cell cell = tileSet.getCellFromPosition(EZInteraction.getXMouse(), EZInteraction.getYMouse());
			
			if (cell.isWalkable())
			{
				// Set the cell to unwalkable first to test pathing
				cell.setWalkable(false);
				List<Cell> path = pathfinding.findPath(new Point(1, 1), new Point(width - 5, height - 5));
				
				// If the path doesn't return anything,
				// that means there is no valid path
				if (path.size() <= 0)
				{
					logger.log("Tower not placed! Reason: " + type.toString() + " would block unit entrance.");
					errorMessages.add("Tower cannot be placed here!");
					cell.setWalkable(true);
					return false;
				}
				
				// The last element in the path would be
				// the end point cell
				Cell lastElement = path.get(path.size() - 1);
				
				// If distance between the last element in the path
				// and the last grid cell is 0, then the tower placement
				// is valid and will not hinder the units pathing to the
				// end point
				if (Point.distance(lastElement.getCenterPoint().x, lastElement.getCenterPoint().y, width - cellSize / 2, height - cellSize / 2) == 0)
				{
					//cell.setWalkable(false);
					towers.add(new Tower(type, new Point(cell.getCenterPoint().x, cell.getCenterPoint().y), tileSet.getTowerAttackCells(cell, type.getAtkRng()), unitList, projectileManager));
					
					// Ensure that we update the path anytime a new
					// tower is placed
					shouldUpdatePath = true;
					
					return true;
				}
				else
				{
					// Reset the cells walkable status to true and stop
					// placement of tower
					cell.setWalkable(true);
					logger.log("Tower not placed! Reason: " + type.toString() + " would block unit path.");
					errorMessages.add("Tower cannot be placed here!");
				}
			}
			else
			{
				logger.log("Tower not placed! Reason: " + "Cell is not walkable.");
				errorMessages.add("Tower cannot be placed here!");
			}
		}
		
		return false;
	}
	
	/**
	 * Handles everything related to tower upgrades
	 */
	private void handleTowerUpgrades()
	{
		if (isTowerMenuOpen)
		{
			// Allow for ESCAPE to cancel tower menu
			if (EZInteraction.wasKeyPressed(KeyEvent.VK_ESCAPE))
			{
				clearUpgradeMenu();
				return;
			}
			
			// Always ensure that if the tower
			// menu is showing, keep it always
			// on top
			upgradeGroup.pullToFront();
			
			// Check if any of the buttons are clicked
			if (EZInteraction.wasMouseLeftButtonPressed())
			{
				int x = EZInteraction.getXMouse();
				int y = EZInteraction.getYMouse();
				
				if (towerMenuUpgrade.isPointInElement(x, y))
				{
					selectedTower.upgrade();
					logger.log("Tower has been upgraded!");
					errorMessages.add("Tower upgraded");
					clearUpgradeMenu();
				}
				else if (towerMenuCancel.isPointInElement(x, y))
				{
					clearUpgradeMenu();
				}
				else if (towerMenuBG.isPointInElement(x, y))
				{
					// Just return here if player clicks
					// the menu background, so that clicks
					// don't get sent to elements behind
					// the upgrade menu
					return;
				}
			}
		}
		
		if (EZInteraction.wasMouseLeftButtonPressed())
		{
			int mouseX = EZInteraction.getXMouse();
			int mouseY = EZInteraction.getYMouse();
			
			for (Tower tower : towers)
			{
				if (tower.getImage().isPointInElement(mouseX, mouseY))
				{
					// If we already have a menu open,
					// close it and change it to the
					// the that is clicked
					if (isTowerMenuOpen)
					{
						clearUpgradeMenu();
					}
					
					isTowerMenuOpen = true;
					selectedTower = tower;
					
					int x = tower.getLocation().x + UPGRADE_WIDTH + UPGRADE_OFFSET > width ? tower.getLocation().x - UPGRADE_OFFSET : tower.getLocation().x + UPGRADE_OFFSET;
					int y = tower.getLocation().y + UPGRADE_HEIGHT + UPGRADE_OFFSET > height ? tower.getLocation().y - UPGRADE_OFFSET : tower.getLocation().y + UPGRADE_OFFSET;
					
					upgradeName.setMsg(selectedTower.toString());
					upgradeDmg.setMsg(Math.round(selectedTower.getDamage() * 100.0) / 100.0 + " Damage");
					
					//upgradeName.translateTo(towerMenuBG.getXCenter() - towerMenuBG.getWidth() / 2 + upgradeName.getWidth(), towerMenuBG.getYCenter() - 50);
					//upgradeDmg.translateTo(towerMenuBG.getXCenter() - towerMenuBG.getWidth() / 2 + upgradeName.getWidth(), towerMenuBG.getYCenter() - 50 + upgradeName.getHeight());
					//upgradeCost.translateTo(towerMenuBG.getXCenter() - towerMenuBG.getWidth() / 2 + upgradeName.getWidth(), towerMenuBG.getYCenter() - 50 + upgradeName.getHeight() + upgradeDmg.getHeight());
					
					// Display attack range around the tower
					switch (selectedTower.getTowerType().getAtkRng())
					{
						case 2:
							towerTwoTile.pullToFront();
							towerTwoTile.translateTo(selectedTower.getLocation().x, selectedTower.getLocation().y);
							towerTwoTile.show();
							break;
							
						case 3:
							towerThreeTile.pullToFront();
							towerThreeTile.translateTo(selectedTower.getLocation().x, selectedTower.getLocation().y);
							towerThreeTile.show();
							break;

						default:
							break;
					}
					
					// Display the tower menu
					upgradeGroup.pullToFront();
					upgradeGroup.show();
					upgradeGroup.translateTo(x, y);
					
					selectedTowerHighlight = EZ.addRectangle((int)tower.getLocation().x, (int)tower.getLocation().getY(), tower.getImage().getWidth(), tower.getImage().getHeight(), Color.red, false);
					
					break;
				}
			}
		}
	}
	
	/**
	 * Clears the tower menu from the screen
	 */
	private void clearUpgradeMenu()
	{
		if (isTowerMenuOpen)
		{
			// Hides all elements of the menu
			upgradeGroup.hide();
			EZ.removeEZElement(selectedTowerHighlight);
			
			// Hides the attack range rectangles
			towerTwoTile.hide();
			towerThreeTile.hide();
			
			// Reset the selected tower to null
			selectedTower = null;
			
			// Make tower menu false
			isTowerMenuOpen = false;
		}
	}
	
	private boolean isTowerMenuOpen = false;
	private Tower selectedTower = null;
	private EZRectangle selectedTowerHighlight = null;
	private EZText upgradeName = null;
	private EZText upgradeDmg = null;
	private EZText upgradeCost = null;
	private EZGroup upgradeGroup = EZ.addGroup();
	private static final int UPGRADE_WIDTH = 200;
	private static final int UPGRADE_HEIGHT = 200;
	private static final int UPGRADE_OFFSET = 120;
	
	private EZImage towerMenuBG;
	private EZImage towerMenuUpgrade;
	private EZImage towerMenuCancel;
	
	/**
	 * Sets up all the elements needed to create the 
	 * tower info / upgrade menu when clicked.
	 */
	public void setupUpgradeGroup()
	{
		int offset = -500;
		
		towerMenuBG = EZ.addImage("bg_towerinfo.png", offset, offset);
		towerMenuBG.hide();
		towerMenuUpgrade = EZ.addImage("BtnUpgrade.png", offset, offset);
		towerMenuUpgrade.hide();
		towerMenuCancel = EZ.addImage("BtnX.png", offset, offset);
		towerMenuCancel.hide();
		
		upgradeName = EZ.addText(offset, offset, "N/A", Color.WHITE, 15);
		upgradeName.hide();
		upgradeDmg = EZ.addText(offset, offset, "N/A", Color.WHITE, 15);
		upgradeDmg.hide();
		upgradeCost = EZ.addText(offset, offset, "N/A", Color.WHITE, 15);
		upgradeCost.hide();
		
		towerMenuBG.translateBy(offset * -1, offset * -1);
		towerMenuUpgrade.translateBy(offset * -1, offset * -1);
		towerMenuCancel.translateBy(offset * -1, offset * -1);
		
		upgradeName.translateBy(offset * -1, offset * -1);
		upgradeDmg.translateBy(offset * -1, offset * -1);
		upgradeCost.translateBy(offset * -1, offset * -1);
		
		towerMenuCancel.scaleTo(0.5);
		towerMenuUpgrade.scaleTo(0.6);
		
		//upgradeName.translateTo(towerMenuBG.getXCenter() - towerMenuBG.getWidth() / 2 + upgradeName.getWidth(), towerMenuBG.getYCenter() - 50);
		//upgradeDmg.translateTo(towerMenuBG.getXCenter() - towerMenuBG.getWidth() / 2 + upgradeName.getWidth(), towerMenuBG.getYCenter() - 50 + upgradeName.getHeight());
		//upgradeCost.translateTo(towerMenuBG.getXCenter() - towerMenuBG.getWidth() / 2 + upgradeName.getWidth(), towerMenuBG.getYCenter() - 50 + upgradeName.getHeight() + upgradeDmg.getHeight());
		upgradeName.translateTo(towerMenuBG.getXCenter(), towerMenuBG.getYCenter() - 40);
		upgradeDmg.translateTo(towerMenuBG.getXCenter(), towerMenuBG.getYCenter() - 40 + upgradeName.getHeight());
		upgradeCost.translateTo(towerMenuBG.getXCenter(), towerMenuBG.getYCenter() - 40 + upgradeName.getHeight() + upgradeDmg.getHeight());
		
		towerMenuUpgrade.translateTo(towerMenuBG.getXCenter(), towerMenuBG.getYCenter() + towerMenuBG.getHeight() / 2 - towerMenuUpgrade.getWorldHeight());
		towerMenuCancel.translateTo(towerMenuBG.getXCenter() + towerMenuBG.getWidth() / 2 - towerMenuCancel.getWorldWidth(), towerMenuBG.getYCenter() - towerMenuBG.getHeight() / 2 + towerMenuCancel.getWorldHeight());
		
		upgradeGroup.addElement(towerMenuBG);
		upgradeGroup.addElement(towerMenuCancel);
		upgradeGroup.addElement(towerMenuUpgrade);
		upgradeGroup.addElement(upgradeName);
		upgradeGroup.addElement(upgradeDmg);
		upgradeGroup.addElement(upgradeCost);
	}
	
	/**
	 * Creates the grid showing the attack range of the different towers
	 */
	public void setupTowerRectangle()
	{
		if (!isTowerRangeRectangleSetup)
		{
			int offset = -500;
			
			int threeTile = 3;
			
			for (int i = -threeTile; i <= threeTile; i++)
			{
				for (int j = -threeTile; j <= threeTile; j++)
				{
					// Skip the center and outter
					// most corners
					if ((i == -threeTile && (j == -threeTile || j == threeTile)) 
							|| (i == threeTile && (j == -threeTile || j == threeTile)) 
							|| (i == 0 && j == 0))
					{
						continue;
					}
					
					// Add the offset so no one sees
					// the rectangle being created on
					// the screen, hide them and then
					// translate them back to the
					// original position
					EZRectangle temp = EZ.addRectangle(i * cellSize + offset, j * cellSize + offset, cellSize, cellSize, Color.RED, false);
					temp.hide();
					temp.translateBy(offset * -1, offset * -1);
					
					towerThreeTile.addElement(temp);
				}
			}
			
			towerThreeTile.translateTo(-500, -500);
			
			int twoTile = 2;
			
			for (int i = -twoTile; i <= twoTile; i++)
			{
				for (int j = -twoTile; j <= twoTile; j++)
				{
					// Skip the center and outter
					// most corners
					if ((i == -twoTile && (j == -twoTile || j == twoTile)) 
							|| (i == twoTile && (j == -twoTile || j == twoTile)) 
							|| (i == 0 && j == 0))
					{
						continue;
					}
					
					// Add the offset so no one sees
					// the rectangle being created on
					// the screen, hide them and then
					// translate them back to the
					// original position
					EZRectangle temp = EZ.addRectangle(i * cellSize + offset, j * cellSize + offset, cellSize, cellSize, Color.RED, false);
					temp.hide();
					temp.translateBy(offset * -1, offset * -1);
					
					towerTwoTile.addElement(temp);
				}
			}
			
			towerTwoTile.translateTo(-500, -500);
			
			isTowerRangeRectangleSetup = true;
		}
	}
}
