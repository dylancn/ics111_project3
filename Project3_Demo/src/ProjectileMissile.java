import java.awt.Point;
import java.awt.geom.Point2D;

import enums.ProjectileType;
import helper.DeltaTime;
import helper.MathHelper;

public class ProjectileMissile extends Projectile
{
	private static final int width = 30;
	private static final int height = 15;
	
	private int xt = 0;
	private int yt = 0;
	private int xb = width;
	private int yb = height;
	
	public ProjectileMissile()
	{
		super(ProjectileType.Missile);
		getProjectileImg().setFocus(0, 0, width, height);
	}
	
	@Override
	public void update()
	{
		if (!isFinished && isActive())
		{
			EZImage projectile = super.getProjectileImg();
			Point projectilePos = new Point(projectile.getXCenter(), projectile.getYCenter());
			Point target = new Point(super.getTarget().getImage().getXCenter(), super.getTarget().getImage().getYCenter());
			
			double distance = MathHelper.getDistance(new Point(target.x, target.y), new Point(projectile.getXCenter(), projectile.getYCenter()));
			if (distance < projectile.getWidth() / 4)
			{
				super.getTarget().causeDamage(super.getParent().getDamage(), super.getType().getElementalType());
				isFinished = true;
				return;
			}
			
			/*if (distanceToNextWayPoint == 0)
			{
				super.setFinished(true);
				super.getTarget().causeDamage(super.getDamage());
				super.destroy();
				
				return;
			}*/
			
			double angle = MathHelper.calcRotationAngleInDegrees(projectilePos, target);
			double distanceToTarget = Point2D.distance(projectilePos.getX(), projectilePos.getY(), target.getX(), target.getY());
			double distanceToNextWayPoint = Math.min(distanceToTarget, DeltaTime.getDeltaTime() / (float)1000 * super.getSpeed());
			
			Point2D.Double destination = MathHelper.createNextWayPoint(distanceToNextWayPoint, angle);
			
			xt += width;
			xb += width;
	
			if (xt >= width * 4)
			{
				xt = 0;
				xb = width;
				yt += height;
				yb += height;
				if (yt >= height * 2)
				{
					xt = 0;
					yt = 0;
					xb = width;
					yb = height;
				}
			}
			
			projectile.setFocus(xt, yt, xb, yb);
			
			// Rotate 
			double rotation = MathHelper.calcRotationAngleInDegrees(new Point(projectile.getXCenter(), projectile.getYCenter()), target);
			if (projectile.getRotation() != rotation)
			{
				projectile.rotateTo(rotation);
			}
			
			projectile.translateBy(destination.x, destination.y);
		}
	}
}
