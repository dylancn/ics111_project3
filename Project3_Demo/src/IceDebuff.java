
import helper.DeltaTime;

public class IceDebuff
{
	// Max 3 stacks of the damage can be applied
	private static final int MAX_STACKS = 3;
	//private static final float stackMultiplier = 0.083f;
	private static final float stackMultiplier = 0.15f;
	// Max duration of slow is ~10secs at 3 stacks
	private static final int duration = 3333;
	private int stacksCount;
	private long expirationTime;
	private boolean expired;
	
	public IceDebuff()
	{
		stacksCount = 0;
	}
	
	public void update()
	{
		if (!expired)
		{
			if (System.currentTimeMillis() > expirationTime)
			{
				expired = true;
				stacksCount = 0;
			}
		}
	}
	
	public boolean isActive()
	{
		return !expired;
	}
	
	/**
	 * Add a stack to the debuff
	 */
	public void addStack()
	{
		expired = false;
		
		// Set the initial expiration time
		if (stacksCount == 0)
		{
			expirationTime = System.currentTimeMillis() + Math.round((float)duration / DeltaTime.getMultiplier());
		}
		
		// If not at max stacks, add
		// one stack. Each stack adds
		// to the expiration time.
		if (stacksCount < MAX_STACKS)
		{
			stacksCount++;
			expirationTime += Math.round((float)duration / DeltaTime.getMultiplier());
		}
	}
	
	/**
	 * 
	 * @return The speed percentage that the unit should walk at
	 */
	public float getSpeedMultiplier()
	{
		return 1.0f - ((float)stacksCount * stackMultiplier);
	}
	
	/**
	 * 
	 * @return True if debuff is at max stacks, false otherwise.
	 */
	public boolean hasMaxStacks()
	{
		return stacksCount == MAX_STACKS;
	}
}
