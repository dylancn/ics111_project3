import java.awt.Point;

import enums.ProjectileType;
import helper.MathHelper;

public class ProjectileLightning extends Projectile
{
	private boolean isFirstShot = true;
	
	private static final int width = 256;
	private static final int height = 160;
	
	private int xt = 0;
	private int yt = 0;
	private int xb = width;
	private int yb = height;
	
	public ProjectileLightning()
	{
		super(ProjectileType.Lightning);
		
		getProjectileImg().setFocus(0, 0, width, height);
	}
	
	
	@Override
	public void update()
	{
		if (!isFinished && isActive())
		{
			if (isFirstShot)
			{
				// Get the distance between offset location and target location
				//double distanceToTarget = MathHelper.getDistance(getOffsetLoc(), new Point(getTarget().getImage().getXCenter(), getTarget().getImage().getYCenter()));
				double distanceToTarget = MathHelper.getDistance(new Point(getParent().getImage().getXCenter(), getParent().getImage().getYCenter()), new Point(getTarget().getImage().getXCenter(), getTarget().getImage().getYCenter()));
	
				// Scale the image so that it's big or small enough to reach the target
				double scaleFactor = distanceToTarget / getProjectileImg().getWidth();
				getProjectileImg().scaleTo(scaleFactor);
				
				// Based on the width of the SCALED picture,
				// find out how much we should offset. Divided
				// by 2 because we only need half of the width
				// due to center place of images.
				int offsetDist = getProjectileImg().getWorldWidth() / 2;
				if (offsetDist > 0)
				{
					offsetDist = offsetDist - (int)(5 * scaleFactor);
					Point offset = MathHelper.calculateOffsetPoint(getParent().getImage().getXCenter(), getParent().getImage().getYCenter(), getTarget().getImage().getXCenter(), getTarget().getImage().getYCenter(), offsetDist);
					getProjectileImg().translateTo(offset.x + getOffsetX(), offset.y + getOffsetY());
				}
				
				double angle = MathHelper.calcRotationAngleInDegrees(new Point(getProjectileImg().getXCenter(), getProjectileImg().getYCenter()), new Point(getTarget().getImage().getXCenter(), getTarget().getImage().getYCenter()));
				getProjectileImg().rotateTo(angle);
				
				for (int i = 0; i < getTargets().size(); i++)
				{
					if (i == 0)
					{
						// Cause full damage to the first target
						getTargets().get(i).causeDamage(getParent().getDamage(), getType().getElementalType());
					}
					else
					{
						// Split the damage based on the number
						// of units in the list
						getTargets().get(i).causeDamage(getParent().getDamage() / (float)getTargets().size(), getType().getElementalType());
					}
				}
				
				isFirstShot = false;
			}
			
			yt += height;
			yb += height;
			if (yt >= height * 10)
			{
				xt = 0;
				yt = 0;
				xb = width;
				yb = height;
				
				isFirstShot = true;
				isFinished = true;
				return;
			}
			
			getProjectileImg().setFocus(xt, yt, xb, yb);
		}
	}
}
