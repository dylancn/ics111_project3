import java.awt.Point;
import java.util.Random;

import enums.EffectsType;

public class ProjectileEffect
{
	private Point location;
	private final EZImage image;
	private final int width;
	private final int height;
	private final EffectsType type;
	
	private int xt = 0;
	private int yt = 0;
	private int xb;
	private int yb;
	private final int rows;
	private final int columns;
	private int counter = 0;
	private final int finishCounter;
	
	private static Random random = new Random();
	
	public ProjectileEffect(EffectsType _type)
	{
		type = _type;
		
		width = type.getSingleWidth();
		height = type.getSingleHeight();
		
		xb = width;
		yb = height;
		rows = type.getRows();
		columns = type.getColumns();
		finishCounter = type.getFinishCounter();
		
		image = EZ.addImage(type.getImgName(), -500, -500);
		image.setFocus(0, 0, width, height);
		
		if (type.equals(EffectsType.FireballExplosion) || type.equals(EffectsType.MissileExplosion))
			image.scaleTo(1.25);
	}
	
	private boolean finished = false;
	private boolean isActive = false;
	
	public void update()
	{
		if (!isFinished() && isActive())
		{
			if (!type.isSingleColumn())
			{
				if (counter > finishCounter)
				{
					finished = true;
					return;
				}
				
				xt += width;
				xb += width;
		
				if (xt >= width * rows)
				{
					xt = 0;
					xb = width;
					yt += height;
					yb += height;
					if (yt >= height * columns)
					{
						xt = 0;
						yt = 0;
						xb = width;
						yb = height;
					}
				}
			}
			else
			{
				if (counter > finishCounter)
				{
					finished = true;
					return;
				}
				
				yt += height;
				yb += height;
				if (yt >= height * rows)
				{
					xt = 0;
					yt = 0;
					xb = width;
					yb = height;
				}
			}
			
			image.setFocus(xt, yt, xb, yb);
			
			counter++;
		}
	}
	
	public void makeActive(Point _location)
	{
		if (!isActive)
		{
			location = _location;
			counter = 0;
			
			image.pullToFront();
			image.translateTo(location.x, location.y);
			
			// These effects can be rotated to
			// give some randomness to them
			if (type.equals(EffectsType.MissileExplosion) || type.equals(EffectsType.FireballExplosion))
			{
				image.rotateTo(random.nextInt(360));
			}
			
			image.show();
			
			isActive = true;
		}
	}
	
	public void makeInactive()
	{
		if (finished)
		{
			image.hide();
			
			finished = false;
			isActive = false;
		}
	}
	
	public boolean isFinished()
	{
		return finished;
	}
	
	public boolean isActive()
	{
		return isActive;
	}
	
	public void destroy()
	{
		EZ.removeEZElement(image);
	}
	
	public EffectsType getEffectsType()
	{
		return type;
	}
}
