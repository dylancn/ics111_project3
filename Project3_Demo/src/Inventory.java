
public class Inventory
{
	private int currency;
	
	public Inventory()
	{
		currency = 0;
	}
	
	public int getCurrencyAmount()
	{
		return currency;
	}
	
	public void addCurrency(int amount)
	{
		currency += amount;
	}
}
