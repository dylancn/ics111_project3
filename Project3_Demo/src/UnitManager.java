import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import enums.UnitType;
import helper.DeltaTime;

public class UnitManager
{
	private static final UnitType[] UNIT_TYPES = UnitType.values();
	
	private final Random random = new Random();
	
	private final int spawnDelay = 1000;
	private int spawnTime = 0;
	private final Player player;
	private final Point unitStartPoint;
	private List<Cell> path;
	private float hpModifier = 1.0f;
	private final List<Unit> unitList = new ArrayList<>();
	
	public UnitManager(Player _player, Point _unitStartPoint)
	{
		player = _player;
		unitStartPoint = _unitStartPoint;
	}
	
	public void update(boolean shouldSpawnUnits, List<Cell> _path)
	{
		path = _path;
		
		// Check if we should spawn a unit
		if (shouldSpawnUnits)
		{
			if (spawnTime > spawnDelay)
			{
				UnitType type = UNIT_TYPES[random.nextInt(UNIT_TYPES.length)];
				
				if (!activateOrMakeNextAvailable(type))
				{
					if (unitList.add(new Unit(type, unitStartPoint, path, hpModifier)))
					{
						spawnTime = 0;
					}
				}
				else
				{
					spawnTime = 0;
				}
			}
			else
			{
				spawnTime += DeltaTime.getDeltaTime();
			}
		}
		
		// Update all the current units
		for (int i = unitList.size() - 1; i >= 0; i--)
		{
			Unit currentUnit = unitList.get(i);
			
			if (!currentUnit.isDead() && !currentUnit.hasReachedEnd())
			{
				currentUnit.updatePath(path);
				currentUnit.moveToNext();
			}
			else
			{
				if (currentUnit.hasReachedEnd() && currentUnit.shouldChangeLife())
				{
					player.changeLives(-1);
					System.out.println(player.getLives());
				}
			}
		}
	}
	
	private boolean activateOrMakeNextAvailable(UnitType type)
	{
		for (Unit unit : unitList)
		{
			if (unit.isDead() && unit.getUnitType().equals(type))
			{
				unit.reincarnate(unitStartPoint, path, hpModifier);
				return true;
			}
		}
		
		return false;
	}
	
	public List<Unit> getUnitList()
	{
		return unitList;
	}
}
