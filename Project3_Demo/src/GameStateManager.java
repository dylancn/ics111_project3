import enums.GameState;

public class GameStateManager
{
	private GameState state = GameState.MainMenu;
	
	public void setState(GameState _state)
	{
		state = _state;
	}
	
	public GameState getState()
	{
		return state;
	}
}
