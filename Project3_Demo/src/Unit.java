import java.awt.Color;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.List;

import enums.ElementalType;
import enums.UnitType;
import helper.DeltaTime;
import helper.MathHelper;

public class Unit
{
	private EZImage unitImage;
	private final UnitType unitType;
	private final FireDot fireDot;
	private final IceDebuff iceDebuff;
	private final float baseSpeed;
	private float speedPerSecond;
	private float hp;
	private float predictiveHp;
	private float hpMultiplier;
	private float startHp;
	private List<Cell> path;
	private boolean isDead;
	private int sequence;
	private int someNumber;
	
	private EZRectangle healthBar;
	private EZRectangle healthPercentage;
	
	private boolean hasReachedEnd = false;
	private boolean shouldChangeLife = false;
	
	private static TileSet tileSet;
	
	private Cell unitCellLocation;
	
	public Unit(UnitType _unitType, Point startLoc, List<Cell> _path, Float _hpModifile)
	{
		unitType = _unitType;
		path = _path;
		
		
		// Put image off the screen first and
		// then move them to where they need
		// to be.
		if (unitType.equals(UnitType.Human_Heavy)){
			unitImage = EZ.addImage("Human_Heavy45_1.png", -700, -700);
			sequence = 9;
			unitImage.setFocus(0,0,45,45);
			height = 45;
			width = 45;
			someNumber = 0;
			}
		if (unitType.equals(UnitType.Human_Light)){
			unitImage = EZ.addImage("Human_Light45.png", -700, -700);
			sequence = 9;
			unitImage.setFocus(0,0,45,45);
			height = 45;
			width = 45;
			someNumber = 0;
			}
		if (unitType.equals(UnitType.Elf_Light)){
			unitImage = EZ.addImage("Elf45_1.png", -700, -700);
			sequence = 9;
			unitImage.setFocus(0,0,45,45);
			height = 45;
			width = 45;
			someNumber = 0;
		}
		if (unitType.equals(UnitType.Orc_Heavy)){
			unitImage = EZ.addImage("Orc_Heavy45_1.png", -700, -700);
			sequence = 9;
			unitImage.setFocus(0,0,45,45);
			height = 45;
			width = 45;
			someNumber = 0;
		}
		if (unitType.equals(UnitType.Orc_Light)){
			unitImage = EZ.addImage("Orc_Light45.png", -700, -700);
			sequence = 9;
			unitImage.setFocus(0,0,45,45);
			height = 45;
			width = 45;
			someNumber = 0;
		}
		
		if (unitType.equals(UnitType.Dragon)){
			unitImage = EZ.addImage("dragon.png", -700, -700);
			sequence = 4;
			unitImage.setFocus(0,0,96,96);
			height = 96;
			width = 96;
			someNumber = 1;
		}
		if (unitType.equals(UnitType.Blue_Dragon)){
			unitImage = EZ.addImage("blueDragon.png", -700, -700);
			sequence = 4;
			unitImage.setFocus(0,0,96,96);
			height = 96;
			width = 96;
			someNumber = 1;
		}
		if (unitType.equals(UnitType.Black_Dragon)){
			unitImage = EZ.addImage("blackDragon.png", -700, -700);
			sequence = 4;
			unitImage.setFocus(0,0,96,96);
			height = 96;
			width = 96;
			someNumber = 1;
		}
		if (unitType.equals(UnitType.Vampire)){
			unitImage = EZ.addImage("vampire.png", -700, -700);
			sequence = 4;
			unitImage.setFocus(0,0,48,48);
			height = 48;
			width = 48;
			someNumber = 1;
		}
		if (unitType.equals(UnitType.Bat)){
			unitImage = EZ.addImage("bat.png", -700, -700);
			sequence = 4;
			unitImage.setFocus(0,0,50,49);
			height = 49;
			width = 50;
			someNumber = 1;
		}
		
		if (unitType.equals(UnitType.Bot)){
			unitImage = EZ.addImage("bot.png", -700, -700);
			sequence = 3;
			unitImage.setFocus(0,0,70,70);
			height = 70;
			width = 70;
			someNumber = 1;
		}
		if (unitType.equals(UnitType.Boss)){
			unitImage = EZ.addImage("boss.png", -700, -700);
			sequence = 3;
			unitImage.setFocus(0,0,97,48);
			height = 48;
			width = 97;
			someNumber = 1;
		}
		
		
		unitImage.translateTo(startLoc.x, startLoc.y);
		
		isDead = false;
		
		fireDot = new FireDot();
		iceDebuff = new IceDebuff();
		
		// If the speed is increased... then the
		// unit health should also increase by
		// the same factor
		baseSpeed = unitType.getMovementSpeed();
		speedPerSecond = baseSpeed;
		hpMultiplier = 1.0f;
		hp = calculateHP();
		predictiveHp = hp;
		
		startHp = hp;
		healthBar = EZ.addRectangle(getImage().getXCenter(), getImage().getYCenter() - getImage().getHeight() / 2, getImage().getWidth(), 5, Color.black, false);
		healthPercentage = EZ.addRectangle(getImage().getXCenter(), getImage().getYCenter() - getImage().getHeight() / 2, getImage().getWidth(), 5, Color.green, true);
	}
	
	private int direction = 1;
	private int oldDirection = 1;
	private int width;
	private int height;
	
	private int xt = 0;
	private int yt = 0;
	private int xb = width;
	private int yb = height;
	private int counter = 0;
	
	private int pathIndex = 0;
	
	// Should probably rename this to update
	public void moveToNext()
	{
		// If the unit has a dot on them,
		// cause that damage here
		if (fireDot.isActive())
		{
			fireDot.update();
			causeDamage(fireDot.getElapsedDamage(), ElementalType.Fire);
			causePredictiveDamage(fireDot.getElapsedDamage(), ElementalType.Fire);
		}
		
		iceDebuff.update();
		
		if (iceDebuff.getSpeedMultiplier() != 1.0f)
		{
			speedPerSecond = baseSpeed * iceDebuff.getSpeedMultiplier();
		}
		else
		{
			speedPerSecond = baseSpeed;
		}
		
		//if (!path.isEmpty())
		if (pathIndex < path.size())
		{
			Point unitPos = new Point(unitImage.getXCenter(), unitImage.getYCenter());
			//System.out.println("Unit Pos: " + unitPos.toString());
			
			Point target = path.get(pathIndex).getCenterPoint();
			//System.out.println("Target Pos: " + target.toString());
			
			double angle = MathHelper.calcRotationAngleInDegrees(unitPos, target);
			//System.out.println("Angle: " + angle);
			
			double distanceToTarget = Point2D.distance(unitPos.getX(), unitPos.getY(), target.getX(), target.getY());
			//System.out.println("Distance to target: " + distanceToTarget);
			
			double distanceToNextWayPoint = Math.min(distanceToTarget, DeltaTime.getDeltaTime() / (float)1000 * speedPerSecond);
			
			Point2D.Double destination = createNextWayPoint(distanceToNextWayPoint, angle);
			//System.out.println("Destination: " + destination);
			
			if (distanceToNextWayPoint == 0)
			{
				pathIndex++;
				return;
			}
			
			// Rotate 
			/*if (unitImage.getRotation() != angle)
			{
				getImage().rotateTo(angle);
			}*/
			
			oldDirection = direction;
			if ((angle >= 0 && angle < 45) || (angle >= 315 && angle < 360))
			{
				if(someNumber == 0){
				direction = 3;
				}
				if(someNumber == 1){
					direction = 2;
				}
				
			}
			else if ((angle >= 45 && angle < 135))
			{
			
				if(someNumber == 0){
					direction = 2;
					}
					if(someNumber == 1){
						direction = 0;
					}
			}
			else if (angle >= 135 && angle < 225)
			{
			
				if(someNumber == 0){
					direction = 1;
					}
					if(someNumber == 1){
						direction = 1;
					}
			}
			else if (angle >= 225 && angle < 315)
			{
				
				if(someNumber == 0){
					direction = 0;
					}
					if(someNumber == 1){
						direction = 3;
					}
			}
			
			if (oldDirection != direction)
			{
				xt = 0;
				yt = height * (direction );
				xb = width;
				yb = yt + height;
			}
			
			if (counter >= 4)
			{
				xt += width;
				xb += width;
				
				counter = 0;
			}
			
			counter++;
			
			if (xt >= width * sequence)
			{
				xt = 0;
				xb = width;
			}
			
			unitImage.setFocus(xt, yt, xb, yb);
			
			//System.out.println("Moving to: " + destination.toString());
			unitImage.translateBy(destination.x, destination.y);
			
			// Set the cell that the unit is in
			unitCellLocation = tileSet.getCellFromPosition(unitImage.getXCenter(), unitImage.getYCenter());
			
			healthBar.translateTo(unitImage.getXCenter(), unitImage.getYCenter() - unitImage.getHeight() / 2);
			healthPercentage.translateTo(unitImage.getXCenter(), unitImage.getYCenter() - unitImage.getHeight() / 2);
			healthPercentage.setWidth((int)((float)hp / startHp * unitImage.getWidth()));
			
			healthBar.pullToFront();
			healthPercentage.pullToFront();
		}
		else
		{
			hasReachedEnd = true;
			shouldChangeLife = true;
			hideElements();
			
			System.out.println("has reached end");
			//setDead(true);
		}
	}
	
	private Point2D.Double createNextWayPoint(double distance, double angle)
	{
		double x = distance * (Math.cos(Math.toRadians(angle)));
		double y = distance * (Math.sin(Math.toRadians(angle)));
		
		return new Point2D.Double(x, y);
	}
	
	private float calculateHP()
	{
		float hp = Math.round(unitType.getHealth() * DeltaTime.getMultiplier() * hpMultiplier);
		
		return hp;
	}
	
	public EZImage getImage()
	{
		return unitImage;
	}
	
	public void updatePath(List<Cell> newPath)
	{
		// If the new path is the same as the current path
		// don't go any further, just return
		if (path.equals(newPath))
		{
			return;
		}
		
 		double closest = -1;
		int index = 0;
		Point closestCellInPathToUnit = path.get(pathIndex).getCenterPoint();
		// Crudely find the cell that's closest to the last cell
		// the unit was traveling to. This can return a bad path
		// where the unit will cut through even though the next 
		// logical cell is somewhere else... however, as long as they
		// get back on the path it shouldn't matter
		for (int i = 0; i < newPath.size(); i++)
		{
			//double distance = MathHelper.getDistance(new Point(getImage().getXCenter(), getImage().getYCenter()), new Point(newPath.get(i).getCenterPoint().x, newPath.get(i).getCenterPoint().y));
			double distance = MathHelper.getDistance(closestCellInPathToUnit, new Point(newPath.get(i).getCenterPoint().x, newPath.get(i).getCenterPoint().y));
			if (closest == -1)
			{
				closest = distance;
				index = i;
			}
			else if (closest > distance)
			{
				closest = distance;
				index = i;
			}
		}
		
		pathIndex = index;
		path = newPath;
	}
	
	/**
	 * @param amount Amount of damage
	 * @param elementalType The Elemental Type of the damage being caused
	 */
	public void causeDamage(float amount, ElementalType elementalType)
	{
		if (elementalType != null)
		{
			if (elementalType.equals(ElementalType.Physical))
			{
				amount = amount * unitType.getPhysicalResist();
			}
			
			if (elementalType.equals(ElementalType.Lightning))
			{
				amount = amount * unitType.getLightningResist();
			}
			
			if (elementalType.equals(ElementalType.Fire))
			{
				amount = amount * unitType.getFireResist();
			}
			
			if (elementalType.equals(ElementalType.Ice))
			{
				amount = amount * unitType.getIceResist();
			}
		}
		
		hp -= amount;
		
		if (hp <= 0)
		{
			setDead(true);
		}
	}
	
	/**
	 * @param amount The amount of damage
	 * @param elementalType The Elemental Type of the damage being caused
	 */
	public void causePredictiveDamage(float amount, ElementalType elementalType)
	{
		if (elementalType != null)
		{
			if (elementalType.equals(ElementalType.Physical))
			{
				amount = amount * unitType.getPhysicalResist();
			}
			
			if (elementalType.equals(ElementalType.Lightning))
			{
				amount = amount * unitType.getLightningResist();
			}
			
			if (elementalType.equals(ElementalType.Fire))
			{
				amount = amount * unitType.getFireResist();
			}
			
			if (elementalType.equals(ElementalType.Ice))
			{
				amount = amount * unitType.getIceResist();
			}
		}
		
		predictiveHp -= amount;
	}
	
	public void addFireDot(float amount)
	{
		fireDot.addStack(amount);
	}
	
	public void addIceDebuff()
	{
		iceDebuff.addStack();
	}
	
	public boolean hasMaxIceStacks()
	{
		return iceDebuff.hasMaxStacks();
	}
	
	/**
	 * 
	 * @return The actual amount of HP remaining on the unit.
	 */
	public float getHp()
	{
		return hp;
	}
	
	/**
	 * 
	 * @return The predicted health of the unit based on the projectiles that have been fired on this unit.
	 */
	public float getPredictiveHp()
	{
		return predictiveHp;
	}
	
	public boolean isDead()
	{
		return isDead;
	}
	
	private void setDead(boolean bool)
	{
		if (bool && !isDead)
		{
			hideElements();
			//destroy();
		}
		
		isDead = bool;
	}
	
	public boolean reincarnate(Point startPoint, List<Cell> _path, float _hpMultiplier)
	{
		if (isDead || hasReachedEnd)
		{
			// Reset Path Index
			pathIndex = 0;
			
			// Reset HP
			hpMultiplier = _hpMultiplier;
			hp = calculateHP();
			predictiveHp = hp;
			startHp = hp;
			
			// Reset path
			path = _path;
			
			// Move all elements to appropriate places
			unitImage.translateTo(startPoint.x, startPoint.y);
			healthBar.translateTo(unitImage.getXCenter(), unitImage.getYCenter() - unitImage.getHeight() / 2);
			healthPercentage.translateTo(unitImage.getXCenter(), unitImage.getYCenter() - unitImage.getHeight() / 2);
			healthPercentage.setWidth((int)((float)hp / startHp * unitImage.getWidth()));
			
			healthBar.pullToFront();
			healthPercentage.pullToFront();
			
			// Show all the elements
			showElements();
			
			// Make this unit alive
			setDead(false);
			hasReachedEnd = false;
			shouldChangeLife = false;
			
			return true;
		}
		
		return false;
	}
	
	public boolean hasReachedEnd()
	{
		return hasReachedEnd;
	}
	
	public boolean shouldChangeLife()
	{
		if (shouldChangeLife)
		{
			shouldChangeLife = false;
			return true;
		}
		
		return false;
	}
	
	public UnitType getUnitType()
	{
		return unitType;
	}
	
	public static void setTileSet(TileSet tset)
	{
		tileSet = tset;
	}
	
	/**
	 * 
	 * @return The cell that the unit is currently on
	 */
	public Cell getUnitCellLoc()
	{
		return unitCellLocation;
	}
	
	/**
	 * 
	 * @return The index of the cell that the unit is currently traveling to in the path
	 */
	public int getIndexOfCurrentPathCell()
	{
		return pathIndex;
	}
	
	/**
	 * 
	 * @return Returns the amount of cells remaining until the unit will have successfully reached the end of the path
	 */
	public int getDistanceToEnd()
	{
		return path.size() - 1 - pathIndex;
	}
	
	private void showElements()
	{
		unitImage.show();
		healthBar.show();
		healthPercentage.show();
	}
	
	private void hideElements()
	{
		unitImage.hide();
		healthBar.hide();
		healthPercentage.hide();
	}
	
	private void destroy()
	{
		EZ.removeEZElement(healthBar);
		EZ.removeEZElement(healthPercentage);
		EZ.removeEZElement(unitImage);
	}
}
