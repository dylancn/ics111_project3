import java.awt.Color;

import enums.GameState;

public class EndScreen
{
	private final int width;
	private final int height;
	private final GameStateManager gsm;
	
	@SuppressWarnings("unused")
	private EZText loseTxt;
	private EZImage toMainMenu;
	private EZImage exit;
	private boolean shouldSetup = true;
	
	public EndScreen(int _width, int _height, GameStateManager _gsm)
	{
		width = _width;
		height = _height;
		gsm = _gsm;
		
		EZ.removeAllEZElements();
	}
	
	public void update()
	{
		if (shouldSetup)
		{
			loseTxt = EZ.addText(width / 2, height / 2, "You lose!", Color.WHITE, 100);
			toMainMenu = EZ.addImage("BtnToMainMenu.png", width / 2, height / 2 - 110);
			exit = EZ.addImage("BtnExit.png", width / 2, height / 2 + 110);
			
			shouldSetup = false;
		}
		
		if (EZInteraction.wasMouseLeftButtonPressed())
		{
			int x = EZInteraction.getXMouse();
			int y = EZInteraction.getYMouse();
			
			if (toMainMenu.isPointInElement(x, y))
			{
				shouldSetup = true;
				gsm.setState(GameState.MainMenu);
			}
			else if (exit.isPointInElement(x, y))
			{
				gsm.setState(GameState.Exit);
			}
		}
		
		EZ.refreshScreen();
	}
}
