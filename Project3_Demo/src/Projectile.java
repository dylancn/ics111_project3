import java.awt.Point;
import java.util.List;

import enums.ProjectileType;

public abstract class Projectile
{
	private Tower parent;
	
	private final ProjectileType type;
	private static final Point startPoint = new Point(-700, -700);
	private int speed;
	private final EZImage projectileImg;
	private Unit targetUnit;
	
	private double x;
	private double y;
	
	protected boolean isFinished = false;
	protected boolean isActive = false;
	
	/**
	 * Must create a new class that extends Projectile to work.
	 * The update function must be overridden with the projectile
	 * specific code.
	 * @param _type The type of the projectile.
	 */
	public Projectile(ProjectileType _type)
	{
		type = _type;
		speed = type.getSpeed();
		
		// Set up the projectiles image and
		// focus if it's needed. Can also be used
		// for anything else that specific projectiles
		// may need.
		switch (type)
		{
			case Laser:
				projectileImg = null;
				break;
				
			case Missile:
				projectileImg = EZ.addImage("Proj_Missile_120x30.png", startPoint.x, startPoint.y);
				break;
				
			case Lightning:
				projectileImg = EZ.addImage("Proj_BoltStrike256.png", startPoint.x, startPoint.y);
				break;
				
			case Fire:
				projectileImg = EZ.addImage("Proj_Fireball32.png", startPoint.x, startPoint.y);
				break;
				
			case Ice:
				projectileImg = EZ.addImage("Proj_Iceball48x114.png", startPoint.x, startPoint.y);
				break;
				
			default:
				projectileImg = null;
				break;
		}
	}
	
	public boolean isFinished()
	{
		return isFinished;
	}
	
	/**
	 * 
	 * @return true if the object is currently being used, false otherwise.
	 */
	public boolean isActive()
	{
		return isActive;
	}
	
	/**
	 * This function makes the object active and sets
	 * the necessary information for the projectile to 
	 * function properly.
	 * @param _parent The EZImage of the tower that this projectile belongs to
	 * @param _targetUnit The unit that this projectile should target.
	 */
	public void makeActive(Tower _parentTower, Unit _targetUnit)
	{
		if (!isActive)
		{
			parent = _parentTower;
			targetUnit = _targetUnit;
			
			// this off sets the projectile so it doesn't appear
			// right in the center of the turret but offset
			// with consideration of its rotation
			x = Math.cos(Math.toRadians(parent.getImage().getRotation())) * (parent.getImage().getWidth() / 2) + parent.getImage().getXCenter();
			y = Math.sin(Math.toRadians(parent.getImage().getRotation())) * (parent.getImage().getHeight() / 2) + parent.getImage().getYCenter();
			
			// Only do this if the projectile is using
			// an EZImage. Some projectiles may not use
			// an image, such as the lasers
			if (projectileImg != null)
			{
				projectileImg.translateTo(x, y);
				
				projectileImg.show();
				projectileImg.pullToFront();
			}
			
			// This must be called ONLY ONCE on every projectile,
			// the first time it's being used on a unit.
			targetUnit.causePredictiveDamage(parent.getDamage(), getType().getElementalType());
			
			isActive = true;
		}
	}
	
	private List<Unit> targetUnits;
	/**
	 * This function makes the object active and sets
	 * the necessary information for the projectile to 
	 * function properly.
	 * @param _parent The EZImage of the tower that this projectile belongs to
	 * @param _targetUnit The units that this projectile should target.
	 */
	public void makeActive(Tower _parentTower, List<Unit> _targetUnits)
	{
		if (!isActive)
		{
			parent = _parentTower;
			targetUnits = _targetUnits;
			targetUnit = targetUnits.get(0);
			
			// this off sets the projectile so it doesn't appear
			// right in the center of the turret but offset
			// with consideration of its rotation
			x = Math.cos(Math.toRadians(parent.getImage().getRotation())) * (parent.getImage().getWidth() / 2) + parent.getImage().getXCenter();
			y = Math.sin(Math.toRadians(parent.getImage().getRotation())) * (parent.getImage().getHeight() / 2) + parent.getImage().getYCenter();
			
			// Only do this if the projectile is using
			// an EZImage. Some projectiles may not use
			// an image, such as the lasers
			if (projectileImg != null)
			{
				projectileImg.translateTo(x, y);
				
				projectileImg.show();
				projectileImg.pullToFront();
			}
			
			// This must be called ONLY ONCE on every projectile,
			// the first time it's being used on a unit.
			for (int i = 0; i < targetUnits.size(); i++)
			{
				if (i == 0)
				{
					// Cause full damage to the first target
					targetUnits.get(i).causePredictiveDamage(parent.getDamage(), getType().getElementalType());
				}
				else
				{
					// Split the damage based on the number
					// of units in the list
					targetUnits.get(i).causePredictiveDamage(parent.getDamage() / (float)targetUnits.size(), getType().getElementalType());
				}
			}
			
			isActive = true;
		}
	}
	
	/**
	 * Makes the projectile inactive and resets
	 * all of the parameters to prepare for future use.
	 */
	public void makeInactive()
	{
		if (projectileImg != null)
		{
			projectileImg.hide();
		}
		
		parent = null;
		targetUnit = null;
		
		x = 0;
		y = 0;
		
		isFinished = false;
		isActive = false;
	}
	
	/**
	 * You MUST override this function in each subclass.
	 * This function is called each iteration of the game
	 * and will update all the logic that the projectile should
	 * have.
	 */
	public abstract void update();
	
	/**
	 * This function will erase the projectile image.
	 * Only call this function if you are sure you won't
	 * use this projectile in the future.
	 */
	public void destroy()
	{
		EZ.removeEZElement(projectileImg);
	}
	
	/**
	 * 
	 * @return the image that the projectile is using.
	 * Some projectiles may not necessarily use an image.
	 */
	public EZImage getProjectileImg()
	{
		return projectileImg;
	}
	
	/**
	 * 
	 * @return the unit that the projectile is trying to hit.
	 */
	public Unit getTarget()
	{
		return targetUnit;
	}
	
	/**
	 * 
	 * @return the unit that the projectile is trying to hit.
	 */
	public List<Unit> getTargets()
	{
		return targetUnits;
	}
	
	/**
	 * 
	 * @return the speed of the projectile
	 */
	public int getSpeed()
	{
		return speed;
	}
	
	/**
	 * 
	 * @return the type of the projectile
	 */
	public ProjectileType getType()
	{
		return type;
	}
	
	/**
	 * 
	 * @return the point that the projectile is offset
	 * with regards to the parents position and angle
	 */
	public Point getOffsetLoc()
	{
		return new Point((int)x, (int)y);
	}
	
	public int getOffsetX()
	{
		return (int)x;
	}
	
	public int getOffsetY()
	{
		return (int)y;
	}
	
	protected Tower getParent()
	{
		return parent;
	}
}
