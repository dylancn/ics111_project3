import java.awt.Color;
import java.awt.Point;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import enums.GameState;
import enums.UnitType;
import helper.DeltaTime;
import helper.Logger;
import helper.StopWatch;

public class Game
{
	private static Random random = new Random();
	private StopWatch stopWatch = new StopWatch();
	
	private boolean ALLOW_DIAGONAL = false;
	
	private List<Unit> unitList = new ArrayList<>();
	
	private final int gameWidth;
	private final int height;
	private final int cellSize;
	private final GameStateManager gsm;
	
	private final TileSet tSet;
	private final Pathfinding pathfinding;
	
	@SuppressWarnings("unused")
	private final EZImage background;
	private final Player player;
	private final Logger logger;
	private final ErrorMessages errorMessages;
	private final UnitManager unitManager;
	private final TowerManager towerManager;
	private final UI ui;
	private final Point unitStartPoint;
	private final Point unitEndPoint;
	
	private final ExecutorService executor = Executors.newSingleThreadExecutor();
	
	//private static final List<String> dirtNames = new ArrayList<>(Arrays.asList("dirt0_32.png", "dirt1_32.png", "dirt2_32.png", "dirt3_32.png"));
	
	public Game(int _width, int _height, int _cellSize, GameStateManager _gsm)
	{
		// If you want to change the speed of the game,
		// either decrease this number to make it slower or
		// increase this number to make it quicker. 
		// 1.0f is the normal speed
		DeltaTime.setMultiplier(2.0f);
		
		gameWidth = _width - 200;
		height = _height;
		cellSize = _cellSize;
		gsm = _gsm;
		
		tSet = new TileSet(gameWidth, height, cellSize);
		pathfinding = new Pathfinding(tSet, ALLOW_DIAGONAL);
		
		unitStartPoint = new Point(cellSize / 2, cellSize / 3);
		unitEndPoint = new Point(gameWidth - 5, height - 5);
		
		background = EZ.addImage("bg_game.png", gameWidth / 2, height / 2);
		
		player = new Player();
		
		logger = new Logger("Game.java");
		logger.setAllowDuplicate(true);
		
		errorMessages = new ErrorMessages(gameWidth);
		
		EZRectangle hide = EZ.addRectangle(gameWidth / 2 + 100, height / 2, gameWidth + 200, height, Color.BLACK, true);
		EZText loading = EZ.addText(gameWidth / 2 + 100, height / 2 - 100, "Loading...", Color.white, 50);
		EZText wait = EZ.addText(gameWidth / 2 + 100, height / 2 + 50, "please wait", Color.WHITE, 50);
		
		// Only call this once per game
		stopWatch.start();
		EZ.setFrameRate(120);
		fillTiles();
		logger.log("Cell image loading took " + stopWatch.getElapsedTime().toMillis() + "ms");
		stopWatch.restart();
		EZ.setFrameRate(65);
		
		unitManager = new UnitManager(player, unitStartPoint);
		unitList = unitManager.getUnitList();
		towerManager = new TowerManager(gameWidth, height, cellSize, tSet, pathfinding, errorMessages, unitList);
		ui = new UI(_width, _height, gsm, errorMessages, towerManager);

		// Set up necessary element groups
		// for the towers
		towerManager.setupTowerRectangle();
		towerManager.setupUpgradeGroup();
		
		// Give Unit class an instance of the TileSet
		// that we're using so it can gain access to methods
		// needed to calculate the cell that it's currently in
		Unit.setTileSet(tSet);
		
		EZ.removeEZElement(hide);
		EZ.removeEZElement(loading);
		EZ.removeEZElement(wait);
		
		// Take in console input without blocking.
		// Don't close the BufferedReader, otherwise,
		// you consequently also close System.in and
		// won't be able to read input for the rest
		// of the life of the program
		BufferedReader bReader = new BufferedReader(new InputStreamReader(System.in));
		executor.submit(() -> {
			while (!lose)
			{
				// If any IO exceptions are caught,
				// just end the loop. Further input 
				// at this point will not be read.
				try
				{
					// This is necessary to avoid being blocked.
					// Without checking if it's ready, the executor
					// would not properly shutdown after losing
					// the game by natural means.
					if (bReader.ready())
					{
						String[] command = bReader.readLine().split(" ");
						float input = 0.0f;
						
						// Catch any invalid parameters
						try
						{
							// Parse string to float, throws exception if invalid
							if (command.length > 1)
								input = Float.parseFloat(command[1]);
							
							switch (command[0])
							{
								case ".lives":
									player.changeLives((long)input);
									logger.log("Changing life by " + (int)input);
									errorMessages.add("You now have: " + player.getLives() + " lives");
									break;
									
								case ".money":
									break;
									
								case ".restart":
									logger.log("Restarting!");
									errorMessages.add("Restarting!!!");
									lose = true;
									break;
									
								case ".speed":
									input = Math.round(input * 100.0f) / 100.0f;
									logger.log("Game speed is now: " + input);
									errorMessages.add("Game speed is now: " + input);
									DeltaTime.setMultiplier(input);
									break;
									
								default:
									logger.log("Invalid command");
									errorMessages.add("Invalid Command");
									break;
							
							}
						}
						catch (Exception e) 
						{
							System.out.println("Invalid command");
							errorMessages.add("Invalid Command");
						}
					}
				}
				catch (Exception e) 
				{
					System.out.println("Cannot read input commands until restart");
					errorMessages.add("Cannot read input commands until restart");
					break;
				}
				
				try
				{
					Thread.sleep(50);
				}
				catch (InterruptedException e)
				{
					// Reset interrupt exception
					Thread.currentThread().interrupt();
					return;
				}
			}
			
		}); 
	}
	
	/**
	 * This function will preload each tile with a 
	 * dirt picture so that the path can be updated
	 * in real time without any slow downs. However,
	 * this comes at the cost of an increased initial
	 * loading time when a new game is created.
	 */
	private void fillTiles()
	{
		List<Cell> cells = tSet.getCells();
		
		for (Cell cell : cells)
		{
			//cell.setCellImg(dirtNames.get(random.nextInt(4)), true);
			cell.setCellImg("tdDirt.png", true);
		}
		
		List<Cell> path = pathfinding.findPath(new Point(1, 1), new Point(gameWidth - 5, height - 5));
		
		oldPath = path;
		currentPath = path;
		
		for (int i = 0; i < cells.size(); i++)
		{
			if (!path.contains(cells.get(i)))
			{
				cells.get(i).hideCellImg();
			}
			else
			{
				cells.get(i).showCellImg();
			}
		}
		
		// The cell that the units are spawned on are
		// not included in the pathfinding... so just manually
		// add that cell
		cells.get(0).showCellImg();
		// Also set it false to walkable to ensure
		// that a tower cannot be placed on the spawning cell
		cells.get(0).setWalkable(false);
	}
	
	private long initialTime = System.nanoTime();
	private final double timeU = 1000000000 / 65;
	private final double timeF = 1000000000 / 65;
	private double deltaU = 0; 
	private double deltaF = 0;
	@SuppressWarnings("unused")
	private int frames = 0;
	@SuppressWarnings("unused")
	private int ticks = 0;
	private long timer = System.currentTimeMillis();
	
	private boolean lose = false;
	private boolean shouldSpawnUnits = false;
	private final StopWatch aStopWatch = new StopWatch();
	private int spawnTime;
	
	private List<Cell> oldPath = new ArrayList<>();
	private List<Cell> currentPath = new ArrayList<>();
	
	public void update()
	{
		// If game state is in Pause then only
		// update the UI to check for interactions
		// and then return after checking
		if (gsm.getState().equals(GameState.Pause))
		{
			ui.update();
			EZ.refreshScreen();
			return;
		}
		
		long currentTime = System.nanoTime();
        deltaU += (currentTime - initialTime) / timeU;
        deltaF += (currentTime - initialTime) / timeF;
        initialTime = currentTime;
		
		// Place all user input related events here
		if (deltaU >= 1)
		{
			// If lose then go to EndScreen.
			// This is the end conditions. 
			// Clean up anything necessary here
			if (lose || player.getLives() <= 0)
			{
				gsm.setState(GameState.EndScreen);
				
				// Make sure this is called when we lose.
				// This executor isn't doing anything crucial
				// where an immediate shutdown would cause harm,
				// so just force a shutdown.
				executor.shutdownNow();
				
				// Just to check whether or not the executor
				// was terminated successfully or not
				try
				{
					if (executor.awaitTermination(2000, TimeUnit.MILLISECONDS))
						logger.log("Successfully terminated executor!");
					else
						logger.log("Unsuccessfully terminated executor!");
				}
				catch (InterruptedException e)
				{
					// Reset interrupted status
					Thread.currentThread().interrupt();
					return;
				}
				
				return;
			}
			
			// Update ui + interactions
			ui.update();
			
			// Control the spawning of units using L for now until
			// something else is implemented to handle enemy spawning
			if (EZInteraction.wasKeyPressed('l'))
			{
				if (shouldSpawnUnits)
					shouldSpawnUnits = false;
				else
					shouldSpawnUnits = true;
				
				logger.log("Spawning units: " + shouldSpawnUnits);
			}
			
			errorMessages.update();
			
			ticks++;
			deltaU--;
		}
		
		// Place all object related events here
		if (deltaF >= 1)
		{
			// Update anything related to towers here
			towerManager.update();
			
			// Hide all cells from old path and
			// show all cells from new path
			if (towerManager.shouldUpdatePath())
			{
				//aStopWatch.start();
				
				currentPath = pathfinding.findPath(unitStartPoint, unitEndPoint);
				
				oldPath.forEach(cell -> cell.hideCellImg());
				currentPath.forEach(cell -> cell.showCellImg());
				
				oldPath = currentPath;
				
				//logger.log("Update path took " + aStopWatch.getElapsedTime().toMillis() + "ms");
				//aStopWatch.reset();
			}
			
			// Update anything related to units here
			unitManager.update(shouldSpawnUnits, currentPath);
			
			frames++;
			deltaF--;
		}
		
		// Reset and output
		if (System.currentTimeMillis() - timer > 1000) 
		{
            logger.log(String.format("UPS: %s, FPS: %s", ticks, frames));
            frames = 0;
            ticks = 0;
            timer += 1000;
        }
		
		EZ.refreshScreen();
	}
}
